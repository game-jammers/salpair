//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace blacktriangles.Salpair
{
	public abstract class CharacterDriver
		: CharacterComponent
	{
		// members ////////////////////////////////////////////////////////////////
        public CharacterControl controller                  { get; private set; }

		// public methods /////////////////////////////////////////////////////////
        public override void Initialize( Character _character )
        {
			base.Initialize( _character );
            controller = character.controller;
        }
	}
}
