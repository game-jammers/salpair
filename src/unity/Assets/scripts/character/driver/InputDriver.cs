//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Salpair
{
    public enum PlayerAction
    {
        MoveX,
        Jump,
        Sprint,
        Nanoplatform,
        Nanoclone,
        Nanoswap,
    }

	public class InputDriver
		: CharacterDriver
	{
        // members /////////////////////////////////////////////////////////////
        public InputRouter<PlayerAction> input              = null;

        // constructor / destructor ///////////////////////////////////////////////
        public override void Initialize( Character _character )
        {
			base.Initialize( _character );
			SetupInput();
		}

        // unity callback //////////////////////////////////////////////////////
		protected virtual void Update()
		{
            if( controller == null ) return;

			// set motion //
			controller.Move( input.GetAxis( PlayerAction.MoveX ) );

            controller.SetSprint( input.GetKey( PlayerAction.Sprint ) );

            if( input.GetKeyDown( PlayerAction.Jump ) )
            {
                controller.Jump();
            }
            else if( input.GetKeyUp( PlayerAction.Jump ) )
            {
                controller.StopJump();
            }


            if( (input.GetKeyDown( PlayerAction.Nanoplatform ) && datamodel.unlocks.nanoplatform) ||
                (input.GetKeyDown( PlayerAction.Nanoclone ) && datamodel.unlocks.nanoclone ) )
            {
                controller.StartNanoability();
            }
            else if( input.GetKey( PlayerAction.Nanoplatform ) || input.GetKey( PlayerAction.Nanoclone ) )
            {
                Vector3 worldMousePos = character.view.cam.ScreenToWorld( input.mousePos );
                controller.UpdateNanoability( worldMousePos );
            }
            else if( input.GetKeyUp( PlayerAction.Nanoplatform ) && datamodel.unlocks.nanoplatform )
            {
                controller.CreateNanoplatform();
            }
            else if( input.GetKeyUp( PlayerAction.Nanoclone ) && datamodel.unlocks.nanoclone )
            {
                controller.CreateNanoclone();
            }

            if( input.GetKey( PlayerAction.Nanoswap ) && datamodel.unlocks.teleport )
            {
                controller.SwapClone();
            }
		}

        // private methods /////////////////////////////////////////////////////
		private void SetupInput()
		{
			input = new InputRouter<PlayerAction>();
			input.Bind( PlayerAction.MoveX, InputAction.FromAxis( KeyCode.A, KeyCode.D ) );
			input.Bind( PlayerAction.Jump, InputAction.FromKey( KeyCode.Space ) );
            input.Bind( PlayerAction.Sprint, InputAction.FromKey( KeyCode.LeftShift ) );
            input.Bind( PlayerAction.Nanoplatform, InputAction.FromKey( KeyCode.Mouse1 ) );
            input.Bind( PlayerAction.Nanoclone, InputAction.FromKey( KeyCode.Mouse0 ) );
            input.Bind( PlayerAction.Nanoswap, InputAction.FromKey( KeyCode.Q ) );
		}
	}
}
