//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles.Salpair
{
    public class Nanoclone
        : Nanoability
    {
        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void OnDestroy()
        {
            //character.controller.OnNanoabilityEnd( this );
        }
    }
}
