//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class Nanocursor
        : Nanoability
    {
        // members /////////////////////////////////////////////////////////////
        public ParticleSystem swarm;

        // public methods //////////////////////////////////////////////////////
        public float MoveTo( Vector3 position, float dt, float speedMod = 1.0f )
        {
            Vector3 diff = position - transform.position;
            Vector3 dir = diff.normalized;
            float dist2 = diff.sqrMagnitude;
            transform.position += dir * character.datamodel.stats.nano.nanoCursorSpeed * dt * speedMod;
            return dist2;
        }

        public void Stop()
        {
            swarm.Stop();
            Destroy( gameObject, 1.0f );
        }
    }
}
