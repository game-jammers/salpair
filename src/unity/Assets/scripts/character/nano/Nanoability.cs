//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles.Salpair
{
    public abstract class Nanoability
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public Character character                          { get; private set; }

        // constructor / destructor ////////////////////////////////////////////
        public void Initialize( Character _character )
        {
            character = _character;
        }
    }
}
