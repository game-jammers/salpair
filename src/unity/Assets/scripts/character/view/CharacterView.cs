//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace blacktriangles.Salpair
{
	public class CharacterView
		: CharacterComponent
		, ICameraController
	{
		// members ////////////////////////////////////////////////////////////////
		public CharacterBody body                           { get; private set; }
		public Animator animator                            { get { return body == null ? null : body.animator; } }
		public btCamera cam                               { get; private set; }
		public bool hasCamera                               { get { return cam != null; } }
        public bool hidden                                  { get; private set; }
        public AudioSource audioSource                      = null;

		// constructor / intiailizer //////////////////////////////////////////////
		public override void Initialize( Character _character )
		{
			base.Initialize( _character );
			RebuildBody();

			if( character.type == Character.Type.LocalPlayer )
			{
				SceneManager.instance.sceneController.RequestCamera( this );
			}
		}

        // public methods //////////////////////////////////////////////////////
        public void Hide()
        {
            body.gameObject.SetActive( false );
            hidden = true;
        }

        public void Show()
        {
            body.gameObject.SetActive( true );
            hidden = false;
        }

		// unity callbacks ////////////////////////////////////////////////////////
		protected virtual void Update()
		{
            UpdateCamera();
			UpdateAnimationParams();
		}

		protected virtual void OnDestroy()
		{
			if( SceneManager.instance != null )
			{
				SceneManager.instance.sceneController.ReleaseCamera( this );
			}
		}

		// callbacks //////////////////////////////////////////////////////////////
		public void OnTakeCamera( btCamera _cam )
		{
			cam = _cam;
			cam.transform.SetParent( body.cameraBone );
			cam.transform.localPosition = Vector3.zero;
			cam.transform.localRotation = Quaternion.identity;
		}

		public void OnReleaseCamera()
		{
			cam = null;
		}

		// private methods ////////////////////////////////////////////////////////
		private void DestroyBody()
		{
			if( body != null )
			{
				Destroy( body );
				body = null;
			}
		}

		private void RebuildBody()
		{
			DestroyBody();

			body = GameObject.Instantiate( datamodel.bodyPrefab ) as CharacterBody;
			body.Initialize( character );
			body.transform.SetParent( transform );
			body.transform.localPosition = Vector3.zero;
			body.transform.localRotation = Quaternion.identity;
            hidden = false;
		}

		private void UpdateCamera()
		{
			if( hasCamera && cam.updateType == btCamera.UpdateType.Manual )
			{
				cam.UpdateCamera( Time.deltaTime );
			}
		}

		private void UpdateAnimationParams()
		{
			if( animator == null ) return;
            if( hidden ) return;
            character.transform.localScale = new Vector3( datamodel.xScale, 1.0f, 1.0f );
            if( datamodel.isGrounded == false )
            {
                if( datamodel.isGrounded.elapsed > datamodel.stats.movement.timeToCrashLand )
                {
                    animator.SetInteger( "land", 2 );
                }
                else if( datamodel.isGrounded.elapsed > datamodel.stats.movement.timeToHeavyLand )
                {
                    animator.SetInteger( "land", 1 );
                }
            }

            animator.SetBool( "sprinting", datamodel.isSprinting );
            animator.SetBool( "walking", datamodel.isWalking );
            animator.SetBool( "grounded", datamodel.isGrounded );
            animator.SetBool( "jumping", datamodel.isJumping );

		}
	}
}
