//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles.Salpair
{

    public class SetLandType
        : StateMachineBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public int landType                                 = 0;

        override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
        {
            animator.SetInteger( "land", landType );
        }
    }
}
