//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles.Salpair
{

    public class AnimLock
        : StateMachineBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
        {
            CharacterBody body = animator.GetComponent<CharacterBody>();
            body.character.datamodel.isAnimationLocked = true;
        }

        override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
        {
            CharacterBody body = animator.GetComponent<CharacterBody>();
            body.character.datamodel.isAnimationLocked = false;
        }
    }
}
