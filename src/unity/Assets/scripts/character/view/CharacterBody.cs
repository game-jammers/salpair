//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace blacktriangles.Salpair
{
	public class CharacterBody
		: CharacterComponent
	{
        // members ////////////////////////////////////////////////////////////////
        public Animator animator                                    = null;
        public Transform cameraBone                                 = null;

        // callbacks //////////////////////////////////////////////////////////////
        public void OnStep()
        {
            Database.audio.PlayRandomClip( "Footsteps", character.transform.position, 1.0f );
        }
  }
}
