//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace blacktriangles.Salpair
{
    public interface StatModifier
    {
        // methods ////////////////////////////////////////////////////////////////
        CharacterStats ApplyModifiers( CharacterStats current, CharacterStats original );
    }
}
