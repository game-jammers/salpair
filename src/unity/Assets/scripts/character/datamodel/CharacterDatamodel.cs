//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections.Generic;

namespace blacktriangles.Salpair
{
    public enum Facing
    {
        East,
        West
    }

	public class CharacterDatamodel
        : CharacterComponent
	{
        // types ///////////////////////////////////////////////////////////////
        [System.Serializable]
        public class Unlocks
        {
            public bool artifact1                           = false;
            public bool artifact2                           = false;
            public bool artifact3                           = false;
            public bool artifact4                           = false;
            public bool artifact5                           = false;
            public bool nanoplatform                        = false;
            public bool nanoclone                           = false;
            public bool teleport                            = false;
        }

        // members ////////////////////////////////////////////////////////////////
        public CharacterStats baseStats                     { get { return _baseStats; } }
        [SerializeField] CharacterStats _baseStats;
        public CharacterStats stats                         { get { return CalculateStats(); } }

        //// prefabs ///////////////////////////////////////
		public CharacterBody bodyPrefab						= null;
        public Nanocursor nanocursorPrefab                  = null;

        public Nanoplatform nanoplatformPrefab              = null;
        public Nanoclone nanoclonePrefab                    = null;

        //// unlocks ///////////////////////////////////////
        public Unlocks unlocks                              = new Unlocks();

        //// nanoabilities /////////////////////////////////
        public Nanocursor nanocursor                        = null;
        public SmoothVector3 nanoTarget                     = null;
        public int nanoTargetSmoothing                      = 10;
        public Nanoability nanoability                      = null;
        public float castStartTime                          = 0.0f;
        public float targetCastTime                         = 0.0f;
        public bool isSwapping                              = false;

        //// movement //////////////////////////////////////
        public SmoothValue<float> xMove                     = null;
        public int xMoveSmoothing                           = 10;
        public SmoothValue<float> yMove                     = null;
        public int yMoveSmoothing                           = 10;

        public bool isAnimationLocked                       = false;
        public bool isWalking                               { get { return !xMove.smoothed.IsNearZero(); } }
        public TimestampedValue<bool> isSprinting           = null;
        public TimestampedValue<bool> isGrounded            = null;
        public bool crashAndBurn                            = false;
        public Facing facing                                = Facing.East;
        public TimestampedValue<bool> isJumping             = null;
        public bool fullJump                                = false;

        public float xScale                                 { get { return facing == Facing.West ? 1.0f : -1.0f; } }

        // constructor / destructor ///////////////////////////////////////////////
        public override void Initialize( Character _character )
        {
            base.Initialize( _character );
            nanoTarget = new SmoothVector3( nanoTargetSmoothing );
            isSprinting = new TimestampedValue<bool>();
            isJumping = new TimestampedValue<bool>();
            isGrounded = new TimestampedValue<bool>();
            isGrounded = true;
            xMove = new SmoothValue<float>( xMoveSmoothing );
            yMove = new SmoothValue<float>( yMoveSmoothing );
        }

        private CharacterStats CalculateStats()
        {
            return baseStats;
        }
    }
}
