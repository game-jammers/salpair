//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles.Salpair
{
    [System.Serializable]
	public struct CharacterStats
	{
        // types ///////////////////////////////////////////////////////////////
        [System.Serializable]
        public struct Movement
        {
            [Range(0.1f,15.0f)] public float moveSpeed;
            [Range(0.0f,10.0f)] public float sprintMod;
            [Range(0.1f,5.0f)]  public float timeToMaxSprint;
            [Range(0.0f,15.0f)] public float moveSpeedFallMod;
            [Range(0.0f,5.0f)] public float timeToHeavyLand;
            [Range(0.0f,5.0f)] public float timeToCrashLand;

            [Range(0.1f,15.0f)] public float fallSpeed;

            [Range(0.1f,5.0f)]  public float hangTime;
            [Range(0.1f,15.0f)] public float jumpForce;
            [Range(0.1f,1.0f)]  public float jumpCutoffTime;
            [Range(0.0f,1.0f)]  public float jumpDelay;
        }

        [System.Serializable]
        public struct Nanoabilities
        {
            [Range(0.0f,10.0f)] public float nanoplatformCastTime;
            [Range(0.0f,10.0f)] public float nanocloneCastTime;
            [Range(1.0f,100.0f)] public float nanoCursorSpeed;
        }

        // members /////////////////////////////////////////////////////////////
        public Movement movement;
        public Nanoabilities nano;
    }
}
