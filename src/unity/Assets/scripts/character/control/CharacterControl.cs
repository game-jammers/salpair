//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class CharacterControl
        : CharacterComponent
	{
        // members /////////////////////////////////////////////////////////////
        public CharacterController unityController          { get; private set; }

        // constructor / destructor ////////////////////////////////////////////
        public override void Initialize( Character _character )
        {
            base.Initialize( _character );
            unityController = GetComponent<CharacterController>();
        }

        // public methods //////////////////////////////////////////////////////
        public void OnPickup( Pickup.Type type )
        {
            switch( type )
            {
                case Pickup.Type.Artifact1: character.datamodel.unlocks.artifact1 = true;break;
                case Pickup.Type.Artifact2: character.datamodel.unlocks.artifact2 = true; break;
                case Pickup.Type.Artifact3: character.datamodel.unlocks.artifact3 = true; break;
                case Pickup.Type.Artifact4: character.datamodel.unlocks.artifact4 = true; break;
                case Pickup.Type.Artifact5: character.datamodel.unlocks.artifact5 = true; break;
                case Pickup.Type.Nanoplatform: character.datamodel.unlocks.nanoplatform = true; break;
                case Pickup.Type.Nanoclone: character.datamodel.unlocks.nanoclone = true; break;
                case Pickup.Type.Teleport: character.datamodel.unlocks.teleport = true; break;
            }

            if( character.datamodel.unlocks.artifact1 &&
                character.datamodel.unlocks.artifact2 &&
                character.datamodel.unlocks.artifact3 &&
                character.datamodel.unlocks.artifact4 &&
                character.datamodel.unlocks.artifact5 )
            {
                SceneController.GetInstance<GameSceneController>().Win();
            }
        }

        public void Move( float movex )
        {
            if( datamodel.isAnimationLocked ) movex = 0.0f;

            datamodel.xMove.Add( movex );
            if( movex < 0.0f )
                datamodel.facing = Facing.West;
            else if( movex > 0.0f )
                datamodel.facing = Facing.East;
        }

        public void Jump()
        {
            if( datamodel.isAnimationLocked ) return;
            if( datamodel.isGrounded )
            {
                Database.audio.PlayRandomClip( "Jump", character.transform.position, 1.0f );
                datamodel.isJumping = true;
                datamodel.fullJump = true;
            }
        }

        public void SetSprint( bool isSprinting )
        {
            if( datamodel.isGrounded && datamodel.isSprinting != isSprinting )
            {
                datamodel.isSprinting = isSprinting;
            }
        }

        public void StopJump()
        {
            if( datamodel.isJumping )
            {
                if( datamodel.isJumping.elapsed < datamodel.stats.movement.jumpCutoffTime )
                {
                    datamodel.fullJump = false;
                }
            }
        }

        public void StartNanoability()
        {
            Vector3 startPos = character.transform.position;
            if( datamodel.nanoability != null )
            {
                startPos = datamodel.nanoability.transform.position;
                Destroy( datamodel.nanoability.gameObject, 0.25f );
                datamodel.nanoability = null;
            }

            character.view.animator.SetBool( "casting", true );
            AudioClip clip = Database.audio.GetRandomClip("NanoSwarmLoop");
            character.view.audioSource.clip = clip;
            character.view.audioSource.volume = 0.3f;
            character.view.audioSource.Play();
            datamodel.isAnimationLocked = true;
            datamodel.nanocursor = Instantiate( datamodel.nanocursorPrefab, startPos + Vector3.up, Quaternion.identity ) as Nanocursor;
            datamodel.nanocursor.Initialize( character );
            datamodel.castStartTime = Time.time;
        }

        public void UpdateNanoability( Vector3 worldPos )
        {
            if( datamodel.nanocursor == null ) return;

            Vector3 curpos = datamodel.nanocursor.transform.position;
            Vector3 diff = worldPos - curpos;
            Vector3 dir = diff.normalized;
            float dist = diff.magnitude;

            RaycastHit hit = default(RaycastHit);

            if( Physics.Raycast( curpos, dir, out hit, dist, EnvironmentLayers.Nanoblocker ) )
            {
                Vector3 targetPos = hit.point - ( dir * 1.0f );
                Debug.DrawLine( curpos, targetPos, Color.red );
                datamodel.nanoTarget.Add( targetPos );
            }
            else
            {
                Debug.DrawLine( curpos, worldPos, Color.green );
                datamodel.nanoTarget.Add( worldPos );
            }
        }

        public void CreateNanoplatform()
        {
            if( CreateNanoability( datamodel.nanoplatformPrefab, datamodel.stats.nano.nanoplatformCastTime ) )
            {
                Database.audio.PlayRandomClip("PlaceNewPlatform", character.transform.position, 1.0f);
            }
        }

        public void CreateNanoclone()
        {
            if( CreateNanoability( datamodel.nanoclonePrefab, datamodel.stats.nano.nanocloneCastTime ) )
            {
                Database.audio.PlayRandomClip("SpawnClone", character.transform.position, 1.0f);
            }
        }

        public void SwapClone()
        {
            if( datamodel.isSwapping == false )
            {
                StartCoroutine( SwapCloneCoroutine() );
            }
        }

        // private methods /////////////////////////////////////////////////////
        private bool CreateNanoability( Nanoability prefab, float minCastTime )
        {
            bool success = false;
            character.view.audioSource.Stop();
            character.view.animator.SetBool( "casting", false );
            Vector3 worldPos = datamodel.nanocursor.transform.position;
            datamodel.isAnimationLocked = false;
            datamodel.nanocursor.Stop();
            datamodel.nanocursor = null;

            if( Time.time - datamodel.castStartTime > minCastTime )
            {
                success = true;
                datamodel.nanoability = Instantiate( prefab, worldPos, Quaternion.identity ) as Nanoability;
                datamodel.nanoability.Initialize( character );
            }
            return success;
        }

        private IEnumerator SwapCloneCoroutine()
        {
            if( datamodel.nanoability is Nanoclone )
            {
                datamodel.isSwapping = true;
                datamodel.isAnimationLocked = true;
                Nanocursor playerCursor = Instantiate( datamodel.nanocursorPrefab, character.transform.position + Vector3.up, Quaternion.identity ) as Nanocursor;
                Nanocursor cloneCursor = Instantiate( datamodel.nanocursorPrefab, datamodel.nanoability.transform.position + Vector3.up, Quaternion.identity ) as Nanocursor;
                playerCursor.Initialize( character );
                cloneCursor.Initialize( character );
                character.view.animator.SetBool( "casting", true );
                character.view.audioSource.clip = Database.audio.GetRandomClip("CloneTransitionalSound");
                character.view.audioSource.volume = 1.0f;
                character.view.audioSource.Play();

                datamodel.nanoability.GetComponent<Character>().view.animator.SetBool( "casting", true );
                yield return new WaitForSeconds( 0.5f );

                character.view.Hide();
                datamodel.nanoability.GetComponent<Character>().view.Hide();

                bool active = true;
                while( active )
                {
                    float playerDist2 = playerCursor.MoveTo( datamodel.nanoability.transform.position + Vector3.up, Time.deltaTime, 0.5f );
                    float cloneDist2 = cloneCursor.MoveTo( character.transform.position + Vector3.up, Time.deltaTime, 0.5f );

                    active = (playerDist2 > 0.5f || cloneDist2 > 0.5f);
                    yield return new WaitForSeconds( 0.0f );
                }


                yield return new WaitForSeconds( 0.25f );

                character.transform.position = playerCursor.transform.position;
                datamodel.nanoability.transform.position = cloneCursor.transform.position;

                yield return new WaitForSeconds( 0.25f );

                character.view.Show();
                datamodel.nanoability.GetComponent<Character>().view.Show();

                character.view.animator.SetBool( "casting", false );
                datamodel.nanoability.GetComponent<Character>().view.animator.SetBool( "casting", false );
                Database.audio.PlayRandomClip("CloneSwap", character.transform.position, 1.0f);
                character.view.audioSource.Stop();

                playerCursor.Stop();
                cloneCursor.Stop();
                datamodel.isAnimationLocked = false;
                datamodel.isSwapping = false;
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void FixedUpdate()
        {
            if( unityController == null ) return;
            Vector3 velocity = Vector3.zero;

            //
            // update nano abilities /
            //
            if( datamodel.nanocursor != null && Time.time - character.datamodel.castStartTime > 0.5f )
            {
                datamodel.nanocursor.MoveTo( datamodel.nanoTarget.smoothed, Time.fixedDeltaTime );
            }

            //
            // update motion //
            //
            if( datamodel.isJumping )
            {
                float hangtime = datamodel.stats.movement.hangTime;
                if( datamodel.fullJump == false ) hangtime *= 0.5f;

                if( datamodel.isJumping.elapsed < datamodel.stats.movement.jumpDelay )
                {
                    // wait for it...
                }
                else if( datamodel.isJumping.elapsed > 0.25f && datamodel.isGrounded )
                {
                    datamodel.isJumping = false;
                }
                else if( datamodel.isJumping.elapsed < hangtime )
                {
                    float jumpPerc = 1.0f - datamodel.isJumping.elapsed / datamodel.stats.movement.hangTime;
                    datamodel.yMove.Set( datamodel.stats.movement.jumpForce * jumpPerc );
                }
                else
                {
                    datamodel.isJumping = false;
                }
            }
            else
            {
                datamodel.yMove.Add( -datamodel.stats.movement.fallSpeed );
            }

            if( datamodel.isWalking )
            {
                float speedMod = datamodel.stats.movement.moveSpeed;
                if( !datamodel.isGrounded )
                {
                    speedMod *= datamodel.stats.movement.moveSpeedFallMod;

                }

                if( datamodel.isSprinting )
                {
                    float perc = btMath.Clamp( datamodel.isSprinting.elapsed / datamodel.stats.movement.timeToMaxSprint, 0.0f, 1.0f );
                    speedMod += datamodel.stats.movement.moveSpeed * datamodel.stats.movement.sprintMod * perc;
                }

                velocity += new Vector3( datamodel.xMove.smoothed, 0.0f, 0.0f ) * speedMod;
            }

            velocity += new Vector3( 0.0f, datamodel.yMove.smoothed, 0.0f );
            unityController.Move( velocity * Time.fixedDeltaTime );
            character.transform.position = character.transform.position.ToVector3XY(0.0f);
            if( datamodel.isGrounded != unityController.isGrounded )
            {
                datamodel.isGrounded = unityController.isGrounded;
            }
        }

        protected virtual void OnControllerColliderHit( ControllerColliderHit hit )
        {
            float angle = Vector3.Angle( hit.normal, Vector3.up );
            if( angle > 135.0f && angle < 225.0f )
            {
                datamodel.isJumping = false;
            }
        }

        // private methods /////////////////////////////////////////////////////
	}
}
