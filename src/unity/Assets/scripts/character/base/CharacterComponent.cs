//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles.Salpair
{
    public abstract class CharacterComponent
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////////
        public Character character                                  { get; private set; }
        public CharacterDatamodel datamodel                         { get; private set; }

        // constructor / destructor ///////////////////////////////////////////////
        public virtual void Initialize( Character _character )
        {
            DebugUtility.Assert( _character != null, "Cannot create a character component without a valid character." );
            character = _character;
            datamodel = character.datamodel;
        }
    }
}
