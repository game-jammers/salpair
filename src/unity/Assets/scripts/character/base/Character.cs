//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections.Generic;

namespace blacktriangles.Salpair
{
	public class Character
		: MonoBehaviour
	{
        // types //////////////////////////////////////////////////////////////////
        public enum Type
        {
            LocalPlayer,
            RemotePlayer,
            LocalNPC,
            RemoteNPC,
            Doll
        }

        // members ////////////////////////////////////////////////////////////////
        public CharacterDatamodel datamodel                 { get { return _datamodel; } }
        public CharacterControl controller                  { get { return _controller; } }
        public CharacterView view                           { get { return _view; } }
        public CharacterDriver driver                       { get { return _driver; } private set { _driver = value; } }

        public Type type                                    { get { return _type; } private set { _type = value; } }

        [SerializeField] Type _type                         = Character.Type.LocalPlayer;
        [SerializeField] bool initializeOnStart             = false;
        [SerializeField] CharacterDatamodel _datamodel      = null;
        [SerializeField] CharacterControl _controller       = null;
        [SerializeField] CharacterView _view                = null;
        [SerializeField] CharacterDriver _driver            = null;

        public IEnumerable<CharacterComponent> components
        {
            get
            {
                // ORDER IS IMPORTANT!
                // datamodel always comes first,
                // controller comes before driver
                // view is always LAST
                yield return datamodel;
                yield return controller;
                yield return driver;
                yield return view;
            }
        }

        // public methods //////////////////////////////////////////////////////
        public void InstallDriver<DriverType>()
            where DriverType: CharacterDriver
        {
            driver = gameObject.AddComponent<DriverType>();
        }

        public void Initialize( Type _type )
        {
            type = _type;
            foreach( CharacterComponent comp in components )
            {
                if( comp != null )
                {
                  comp.Initialize( this );
                }
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            if( initializeOnStart )
            {
                Initialize( _type );
            }
        }
    }
}
