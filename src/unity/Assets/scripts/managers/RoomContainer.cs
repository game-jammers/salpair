//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class RoomContainer
	{
        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get; private set; }
        public IntVec2 centerRoom                           = IntVec2.zero;
        public List<Room> rooms                             = new List<Room>();

        // constructor / destructor ////////////////////////////////////////////
        public RoomContainer( GameSceneController _sceneController )
        {
            sceneController = _sceneController;
        }

        public static Room LoadRoomForCell( IntVec2 roomId )
        {
            Room result = null;
            string name = System.String.Format( "environments/Environment_{0}_{1}", roomId.x, roomId.y );
            GameObject roomGo = Resources.Load<GameObject>( name ) as GameObject;
            if( roomGo != null )
            {
                result = GameObject.Instantiate( roomGo ).GetComponent<Room>();
                DebugUtility.Assert( result != null , name + " IS NOT A ROOM!" );
            }
            return result;
        }

        // public methods //////////////////////////////////////////////////////
        public void Reload()
        {
            foreach( Room room in rooms )
            {
                GameObject.Destroy( room.gameObject );
            }

            rooms.Clear();

            LoadRoomAsync( centerRoom );
        }

        public void LoadRoomAsync( IntVec2 roomId )
        {
            LoadSceneAsync( roomId );
            DirectionHelper.ForEachDir( (dir)=> {
                IntVec2 neighborId = centerRoom + DirectionHelper.ToOffset(dir);
                LoadSceneAsync( neighborId );
            });
        }

        public void LoadSceneAsync( IntVec2 roomId )
        {
            if( IsRoomLoaded( roomId ) ) return;
            Room room = LoadRoomForCell( roomId );

            if( room != null )
            {
                room.id = roomId;
                IntVec2 offset = roomId - centerRoom;
                room.SetPositionFromOffset( offset );
                rooms.Add( room );
            }
        }

        public void ChangeRoom( Direction direction )
        {
            DirectionHelper.ForEachDir( (dir)=>{
                if( dir == direction ) return;
                UnloadRoom( centerRoom + DirectionHelper.ToOffset(dir) );
            });

            IntVec2 offset = DirectionHelper.ToOffset( direction );
            centerRoom += offset;

            Vector3 recenterOffset = new Vector3( offset.x * Room.kRoomSize.x, offset.y * Room.kRoomSize.y, 0.0f );
            foreach( Room room in rooms )
            {
                room.transform.position -= recenterOffset;
            }
            sceneController.Recenter( recenterOffset );

            LoadRoomAsync( centerRoom );
        }

        public void UnloadRoom( IntVec2 roomId )
        {
            Room room = PullRoom( roomId );
            if( room != null )
            {
                GameObject.Destroy( room.gameObject );
            }
        }

        public Room PullRoom( IntVec2 roomId )
        {
            Room result = GetRoom( roomId );
            if( result != null )
            {
                rooms.Remove( result );
            }
            return result;
        }

        public Room GetRoom( IntVec2 roomId )
        {
            foreach( Room room in rooms )
            {
                if( room.id == roomId ) return room;
            }

            return null;
        }

        public bool IsRoomLoaded( IntVec2 roomId )
        {
            return (GetRoom( roomId ) != null);
        }
    }
}
