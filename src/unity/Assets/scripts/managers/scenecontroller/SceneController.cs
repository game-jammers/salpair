//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Salpair
{
	public class SceneController
		: BaseSceneController
	{
    // members ////////////////////////////////////////////////////////////////
    public static SceneController instance                  { get; private set; }
    public Vector3 sceneCamPos                              { get { return sceneCam.unityCamera.transform.position; } }

    // public methods /////////////////////////////////////////////////////////
    public static T GetInstance<T>()
      where T: SceneController
    {
      return instance as T;
    }

    // unity callbacks ////////////////////////////////////////////////////////
    protected virtual void Awake()
    {
      instance = this;
      GameManager.EnsureExists();
      SceneManager.EnsureExists();
      SceneManager.instance.OnSceneControllerLoaded( instance );
    }
  };
}
