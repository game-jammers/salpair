//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class EndingSceneController
		: SceneController
	{
        // members /////////////////////////////////////////////////////////////
        public BackgroundManager background                 = null;
        public Animator characterAvatar                     = null;
        public UIElement whiteout                           = null;
        // public methods //////////////////////////////////////////////////////
        public void Start()
        {
            whiteout.Fade( false, 3.0f, ()=>{} );
        }
    };
}
