//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class GameSceneController
		: SceneController
	{
        // members /////////////////////////////////////////////////////////////
        public Character player                             = null;
        public BackgroundManager background                 = null;
        public RoomContainer rooms                          = null;
        public UIElement fadeToWhite                        = null;

        [SerializeField] bool autoloadRooms                 = true;

        // public methods //////////////////////////////////////////////////////
        public void Recenter( Vector3 offset )
        {
            player.transform.position -= offset;
            sceneCam.transform.position -= offset;
        }

        public void Win()
        {
            player.datamodel.isAnimationLocked = true;
            fadeToWhite.Fade( true, 5.0f, ()=>{
                UnityEngine.SceneManagement.SceneManager.LoadScene( 2 );
            });
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            if( autoloadRooms )
            {
                rooms = new RoomContainer( this );
                rooms.Reload();
            }
        }

        protected virtual void Update()
        {
            if( autoloadRooms == false ) return;
            if( player.transform.position.x > Room.kHalfRoomSize.x )
            {
                rooms.ChangeRoom( Direction.East );
            }
            else if( player.transform.position.x < -Room.kHalfRoomSize.x )
            {
                rooms.ChangeRoom( Direction.West );
            }

            if( player.transform.position.y > Room.kHalfRoomSize.y )
            {
                rooms.ChangeRoom( Direction.North );
            }
            else if( player.transform.position.y < -Room.kHalfRoomSize.y )
            {
                rooms.ChangeRoom( Direction.South );
            }
        }
    };
}
