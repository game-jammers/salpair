//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class MainMenuSceneController
		: SceneController
	{
        // members /////////////////////////////////////////////////////////////
        public BackgroundManager background                 = null;
        public Animator characterAvatar                     = null;


        // public methods //////////////////////////////////////////////////////
        public void StartGame()
        {
            StartCoroutine( StartGameCoroutine() );
        }

        private IEnumerator StartGameCoroutine()
        {
            Database.audio.PlayRandomClip( "Crouch", sceneCamPos, 1.0f );
            characterAvatar.SetTrigger( "start" );
            yield return new WaitForSeconds( 1.0f );
            UnityEngine.SceneManagement.SceneManager.LoadScene( 1 );
        }
    };
}
