//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Salpair
{
	public class GameCamera
		: btCamera
	{
        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get { return SceneController.GetInstance<GameSceneController>(); } }
        public Vector2 minScrollBoundaryPerc                = new Vector2( 10.0f, 0.0f );
        public Vector2 maxScrollBoundaryPerc                = new Vector2( 80.0f, 90.0f );

        private Vector2 minScrollBoundary                   = Vector2.zero;
        private Vector2 maxScrollBoundary                   = Vector2.zero;
        private Vector3 screenPos                           = Vector2.zero;

        private Vector3 cameraTarget                        = Vector3.zero;
        private Vector3 scale                               = Vector3.zero;

        // public methods //////////////////////////////////////////////////////
        public override void UpdateCamera( float dt )
	    {
            screenPos = WorldToScreenPoint( sceneController.player.transform.position );

            minScrollBoundary = new Vector2( Screen.width * minScrollBoundaryPerc.x, Screen.height * minScrollBoundaryPerc.y );
            maxScrollBoundary = new Vector2( Screen.width * maxScrollBoundaryPerc.x, Screen.height * maxScrollBoundaryPerc.y );

            bool moveCamera = true;
            Vector3 deltaMove = Vector3.zero;
            if( screenPos.x < minScrollBoundary.x )
            {
                deltaMove.x = screenPos.x - minScrollBoundary.x;
                moveCamera = true;
            }
            else if( screenPos.x > maxScrollBoundary.x )
            {
                deltaMove.x = screenPos.x - maxScrollBoundary.x;
                moveCamera = true;
            }

            if( screenPos.y < minScrollBoundary.y )
            {
                deltaMove.y = screenPos.y - minScrollBoundary.y;
                moveCamera = true;
            }
            else if( screenPos.y > maxScrollBoundary.y )
            {
                deltaMove.y = screenPos.y - maxScrollBoundary.y;
                moveCamera = true;
            }

            if( moveCamera )
            {
                cameraTarget = unityCamera.ScreenToWorldPoint( screenPos + deltaMove );
                Vector3 diff = cameraTarget - transform.position;
                Vector3 dir = diff.normalized;
                float dist2 = diff.sqrMagnitude;

                scale = new Vector3( dir.x * Mathf.Abs(diff.x / (Screen.width / 4.0f)), dir.y * Mathf.Abs(diff.y / (Screen.height / 16.0f)), 0.0f ) * speed;

                transform.position += scale;
                sceneController.background.Scroll( scale );
            }

	    }
	}
}
