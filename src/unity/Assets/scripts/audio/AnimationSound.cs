//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles.Salpair
{

    public class AnimationSound
        : StateMachineBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public string clipName                              = "Jump";
        public float volume                                 = 0.5f;
        public float delay                                  = 0.0f;

        override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
        {
            if( delay <= 0.0f )
            {
                PlaySound( animator );
            }
            else
            {
                GameManager.instance.DelayCall( delay, ()=>{ PlaySound( animator ); } );
            }
        }

        private void PlaySound( Animator animator )
        {
            Database.audio.PlayRandomClip( clipName, animator.transform.position, volume );
        }
    }
}
