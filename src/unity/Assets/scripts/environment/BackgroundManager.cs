//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class BackgroundManager
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public BackgroundLayer[] layers                     { get; private set; }

        // public methods //////////////////////////////////////////////////////
        public void Scroll( Vector2 scrollAmount )
        {
            foreach( BackgroundLayer layer in layers )
            {
                layer.Scroll( scrollAmount );
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            layers = transform.GetComponentsInChildren<BackgroundLayer>();
        }
    }
}
