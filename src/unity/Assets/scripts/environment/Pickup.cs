//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Salpair
{
    public class Pickup
        : MonoBehaviour
    {
        // types ///////////////////////////////////////////////////////////////
        public enum Type
        {
            Artifact1,
            Artifact2,
            Artifact3,
            Artifact4,
            Artifact5,
            Nanoplatform,
            Nanoclone,
            Teleport
        }

        // members /////////////////////////////////////////////////////////////
        public Type type                                    = Type.Artifact1;
        public GameObject pickupEffect                      = null;
        public GameObject pickupItem                        = null;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void OnTriggerEnter( Collider collider )
        {
            Character character = collider.GetComponent<Character>();
            if( character == null || character.type != Character.Type.LocalPlayer ) return;

            character.controller.OnPickup( type );
            if(pickupItem)
            {
                BagHolder.AddItemToAnyBag((Instantiate(pickupItem).GetComponent<Item>()));
            }
            else
            {
                Debug.LogWarning("There was No Item linked to the Pickup.");
            }

            if( pickupEffect != null )
            {
                Instantiate( pickupEffect, transform.position, Quaternion.identity );
            }
            Destroy( gameObject, 0.25f );
        }
    }
}
