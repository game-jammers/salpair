//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class Room
        : MonoBehaviour
    {
        // constants ///////////////////////////////////////////////////////////
        public static readonly Vector2 kRoomSize            = new Vector2( 100.0f, 50.0f );
        public static readonly Vector2 kHalfRoomSize        = new Vector2( kRoomSize.x / 2.0f , kRoomSize.y / 2.0f );

        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get; private set; }
        public IntVec2 id                                   = IntVec2.zero;

        // public methods //////////////////////////////////////////////////////
        public void SetPositionFromOffset( IntVec2 offset )
        {
            transform.position = new Vector3( offset.x * kRoomSize.x, offset.y * kRoomSize.y, 0.0f );
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            sceneController = SceneController.GetInstance<GameSceneController>();
        }
    }
}
