//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public interface ITrigger
    {
        void Trigger();
    }

    public interface ITriggerable
    {
        void OnTrigger( ITrigger trigger );
    }
}
