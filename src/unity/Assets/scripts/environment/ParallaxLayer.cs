//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class ParallaxLayer
        : BackgroundLayer
    {
        // members /////////////////////////////////////////////////////////////
        public Vector2 currentScroll                        { get; private set; }
        public Vector2 scrollSpeed                          = Vector2.one;
        [SerializeField] bool autoScroll                    = false;
        [SerializeField] Color tint                         = Color.white;
        [SerializeField] Texture image                      = null;
        [SerializeField] Renderer rend                      = null;


        // public methods //////////////////////////////////////////////////////
        public override void Scroll( Vector2 scrollAmount )
        {
            if( rend == null ) return;
            if( rend.materials == null ) return;
            Material[] instanceMaterials = rend.materials;
            ForEachMaterial( (material)=>{
                float deltaScrollX = (scrollAmount.x * scrollSpeed.x) % 10.0f;
                float deltaScrollY = (scrollAmount.y * scrollSpeed.y) % 10.0f;
                currentScroll += new Vector2( deltaScrollX, deltaScrollY );
                material.SetTextureOffset("_MainTex", currentScroll);
            });
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            if( rend == null )
            {
                rend = GetComponent<Renderer>();
            }

            ForEachMaterial( (mat)=>{
                mat.SetColor( "_Color", tint );
                mat.SetTexture( "_MainTex", image );
            });
            currentScroll = Vector2.zero;
        }

        protected virtual void Update()
        {
            if( autoScroll )
            {
                Scroll( Vector2.one * Time.deltaTime );
            }
        }

        // private methods /////////////////////////////////////////////////////
        private void ForEachMaterial( System.Action<Material> function )
        {
            if( rend == null ) return;
            if( rend.materials == null ) return;
            Material[] instanceMaterials = rend.materials;
            foreach( Material material in instanceMaterials )
            {
                function( material );
            }
        }
    }
}
