//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class BackgroundTileLayer
        : BackgroundLayer
    {
        // members /////////////////////////////////////////////////////////////
        public BackgroundTile[] tileset;
        public IntVec2 maxTiles                             = IntVec2.one;
        public Vector2 scrollSpeed                          = Vector2.one;
        public Vector2 totalSize                            { get { return new Vector2( tileSize.x * maxTiles.x, tileSize.y * maxTiles.y ); } }
        public Vector2 autoscroll                           = Vector2.zero;
        public bool autoscrollOnly                          = false;

        [SerializeField] Vector2 tileSize                   = new Vector2( 50.0f, 25.0f );
        private GameObject root                             = null;
        private GameObject center                           = null;
        private BackgroundTile[,] tiles                     = null;

        private IntVec2 indexOffset                         = IntVec2.zero;

        // public methods //////////////////////////////////////////////////////
        public override void Scroll( Vector2 scroll )
        {
            if( autoscrollOnly == false)
            {
                DoScroll( scroll );
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            tiles = new BackgroundTile[ maxTiles.x, maxTiles.y ];
            indexOffset.x = (int)btMath.Floor( maxTiles.x / 2.0f );
            indexOffset.y = (int)btMath.Floor( maxTiles.y / 2.0f );

            center = new GameObject( "Center" );
            center.transform.SetParent( transform, true );
            center.transform.localPosition = totalSize.ToVector3XY() / -2.0f;
            center.transform.rotation = Quaternion.identity;

            root = new GameObject( "LayerRoot" );
            root.transform.SetParent( center.transform, true );
            root.transform.localPosition = Vector3.zero;
            root.transform.rotation = Quaternion.identity;

            for( int y = 0; y < maxTiles.y; ++y )
            {
                for( int x = 0; x < maxTiles.x; ++x )
                {
                    AddRandomTile( x, y );
                }
            }

            if( autoscroll.sqrMagnitude > 0.0f )
            {
                StartCoroutine( AutoScroll() );
            }
        }

        // private methods /////////////////////////////////////////////////////
        private void DoScroll( Vector2 scroll )
        {
            Vector2 bounds = tileSize;
            Vector3 scrollAmount = scroll.VectorScale(scrollSpeed).ToVector3XY() * -1.0f;
            root.transform.localPosition += scrollAmount;

            if( tileSize.x > 1 )
            {
                if( root.transform.localPosition.x > bounds.x )
                {
                    ShiftLeft();
                    root.transform.localPosition -= new Vector3( bounds.x, 0.0f, 0.0f );
                }

                if( root.transform.localPosition.x < -bounds.x )
                {
                    ShiftRight();
                    root.transform.localPosition += new Vector3( bounds.x, 0.0f, 0.0f );
                }
            }

            if( tileSize.y > 1 )
            {
                 if( root.transform.localPosition.y > bounds.y )
                 {
                     ShiftUp();
                     root.transform.localPosition -= new Vector3( 0.0f, bounds.y, 0.0f );
                 }

                 if( root.transform.localPosition.y < -bounds.y )
                 {
                     ShiftDown();
                     root.transform.localPosition += new Vector3( 0.0f, bounds.y, 0.0f );
                 }
            }
        }

        private IEnumerator AutoScroll()
        {
            while( true )
            {
                DoScroll( autoscroll * Time.deltaTime );
                yield return new WaitForSeconds( 0.0f );
            }
        }

        private IntVec2 ToIndex( int x, int y )
        {
            return new IntVec2( x + indexOffset.x - 1, y + indexOffset.y - 1 );
        }

        private BackgroundTile GetCenterTile()
        {
            return tiles[indexOffset.x,indexOffset.y];
        }

        private BackgroundTile AddRandomTile( int x, int y)
        {
            BackgroundTile result = Instantiate( tileset.Random(), Vector3.zero, Quaternion.identity ) as BackgroundTile;
            result.transform.SetParent( root.transform, true );
            result.name = System.String.Format( "Tile {0},{1}", x, y );
            PlaceTile( result, x, y );
            return result;
        }

        private void PlaceTile( BackgroundTile tile, int x, int y )
        {
            IntVec2 index = new IntVec2( x, y );
            tiles[index.x,index.y] = tile;
            Vector2 offset = new Vector2( x * tileSize.x, y * tileSize.y );
            tile.transform.localPosition = offset.ToVector3XY();
        }

        private void ShiftLeft()
        {
            for( int y = 0; y < maxTiles.y; ++y )
            {
                BackgroundTile looper = tiles[0,y];

                for( int x = 1; x < maxTiles.x; ++x )
                {
                    BackgroundTile t = tiles[x,y];
                    PlaceTile( t, x-1, y );
                }

                PlaceTile(looper, maxTiles.x-1, y );
            }
        }

        private void ShiftRight()
        {
            for( int y = 0; y < maxTiles.y; ++y )
            {
                BackgroundTile looper = tiles[maxTiles.x-1,y];

                for( int x = maxTiles.x-2; x >= 0; --x )
                {
                    BackgroundTile t = tiles[x,y];
                    PlaceTile( t, x+1, y );
                }

                PlaceTile(looper, 0, y );
            }
        }

        private void ShiftDown()
        {
            for( int x = 0; x < maxTiles.x; ++x )
            {
                BackgroundTile looper = tiles[x,0];

                for( int y = 1; y < maxTiles.y; ++y )
                {
                    BackgroundTile t = tiles[x,y];
                    PlaceTile( t, x, y-1 );
                }

                PlaceTile(looper, x, maxTiles.y - 1 );
            }
        }

        private void ShiftUp()
        {
            for( int x = 0; x < maxTiles.x; ++x )
            {
                BackgroundTile looper = tiles[x,maxTiles.y-1];

                for( int y = maxTiles.y-2; y >= 0; --y )
                {
                    BackgroundTile t = tiles[x,y];
                    PlaceTile( t, x, y+1 );
                }

                PlaceTile(looper, x, 0 );
            }
        }
    }
}
