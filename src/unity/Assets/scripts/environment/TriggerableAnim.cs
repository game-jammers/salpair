//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class TriggerableAnim
        : MonoBehaviour
        , ITriggerable
    {
        // members /////////////////////////////////////////////////////////////
        public Animator animator;
        public string triggerName;

        // public methods //////////////////////////////////////////////////////
        public void OnTrigger( ITrigger trigger )
        {
            animator.SetTrigger( triggerName );
        }
    }
}
