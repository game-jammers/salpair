//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }

    public static class DirectionHelper
    {
        public static Direction Reverse( Direction dir )
        {
            switch( dir )
            {
                case Direction.North: return Direction.South;
                case Direction.East: return Direction.West;
                case Direction.South: return Direction.North;

                default:
                case Direction.West: return Direction.East;
            }
        }

        public static IntVec2 ToOffset( Direction dir )
        {
            switch( dir )
            {
                case Direction.North: return new IntVec2( 0, 1 );
                case Direction.East: return new IntVec2( 1, 0 );
                case Direction.South: return new IntVec2( 0, -1 );

                default:
                case Direction.West: return new IntVec2( -1, 0 );
            }
        }

        public static void ForEachDir( System.Action<Direction> func )
        {
            foreach( Direction dir in System.Enum.GetValues( typeof( Direction ) ) )
            {
                func( dir );
            }
        }
    }
}
