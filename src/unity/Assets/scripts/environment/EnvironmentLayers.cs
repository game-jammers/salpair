//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;

namespace blacktriangles.Salpair
{
	public static class EnvironmentLayers
	{
		// contants ///////////////////////////////////////////////////////////
		public static readonly string kNanoblockerName      = "Nanoblock";

		public static readonly LayerMask Nanoblocker        = LayerMask.GetMask( kNanoblockerName );
	}

	public static class EnvironmentTags
	{
		public static readonly string Player				= "Player";
        public static readonly string Enemy 				= "Enemy";
		public static readonly string Trigger				= "Trigger";
	}
}
