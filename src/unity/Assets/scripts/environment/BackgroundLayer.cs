//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public abstract class BackgroundLayer
        : MonoBehaviour
    {
        // public methods //////////////////////////////////////////////////////
        public abstract void Scroll( Vector2 scroll );
    }
}
