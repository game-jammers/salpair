﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    Light CompLight;
    int cooldown = 0;
    int cooldownMax = 4;

    private void Awake()
    {
        CompLight = GetComponent<Light>();
    }
	// Update is called once per frame
	void Update ()
    {
        if (cooldown <= 0)
        {
            if (Random.Range(0, 30) == 0)
            {
                CompLight.enabled = false;
                cooldown = cooldownMax;
            }
            else
            {
                CompLight.enabled = true;
            }
        }
        cooldown--;

    }
}
