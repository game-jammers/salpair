﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorAccessor : MonoBehaviour {

    //variables left public for easy debug and setting
    public int passKey = 1;
    public int playerKey;
    public GameObject thePlayer;
    ColorContainers colorContainers;
    public float testDown = -1f;
    private CharacterController velocityTester;
    public GameObject telportTarget;
    public GameObject door;
    public Animator doorKey;
    public float localVel;

	void OnTriggerEnter (Collider col)
    {
        //Get the player on trigger, grab the script with the color int, assign int to this script for testing
        thePlayer = col.gameObject;
        colorContainers = thePlayer.GetComponent<ColorContainers>();
        playerKey = colorContainers.colorLevel;
        velocityTester = thePlayer.GetComponent<CharacterController>();
        //get the rigidbody's vector3 velocity
        localVel = velocityTester.velocity.y;

        //check the keys for the puzzle gate
        CheckKeys();
    }

    void CheckKeys()
    {
        if (passKey == playerKey)
        {

            //this is the solution.  Either teleport or unlocks or plays some sort of animation here.  Mostly used for progression.
            Debug.Log("this will open");
            //basic teleport
            thePlayer.transform.position = telportTarget.transform.position;

            ////animate the door
            //doorKey = door.GetComponent<Animator>();
            //doorKey.SetBool("unlock", true);

            ////deactivate trigger
            //this.gameObject.SetActive(false);
        }

        ////this is for button press on jumping.  The script doesn't really work but the logic is to test the current downard velocity vs a set number to push the button.
        //if (passKey == playerKey && localVel <= testDown)
        //{
        //    Debug.Log("button is pressed");

        //    //    //animate the door
        //    //    doorKey = door.GetComponent<Animator>();
        //    //    doorKey.SetBool("unlock", true);

        //    //    //deactivate trigger
        //    //    this.gameObject.SetActive(false);
        //}
    }


}
