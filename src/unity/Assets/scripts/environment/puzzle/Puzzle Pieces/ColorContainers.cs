﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorContainers : MonoBehaviour {


    //leaving variables public so changes can be observed and debugged easily in inspector
    public Color maxColor;
    public Color newColor;
    public int colorLevel;
    public int colorLevelNew;
    public float lerpSpeed = 0.5f;

    //called on start of program
    void Start()
    {
        //set current color value
        if (colorLevel == 0)
        {
            maxColor = Color.white;
        }
        else if (colorLevel == 1)
        {
            maxColor = Color.blue;
        }
        else if (colorLevel == 2)
        {
            maxColor = Color.red;
        }
        else if (colorLevel == 3)
        {
            maxColor = Color.yellow;
        }
    }


    //Update is called once per frame
    void Update()
    {
        //check to see if the color needs to change by testing the int value
        if (colorLevelNew != colorLevel)
        {
            ColorTest();
        }
    }

    public void ColorTest()
    {
       //Take starting color and lerp it to the new color
        if (colorLevelNew == 1)
        {
            while (newColor != maxColor)
            {
                //instead of a sudden change, it will lerp over time
                newColor = Color.Lerp(maxColor, Color.blue, lerpSpeed);
                maxColor = newColor;
            }
            colorLevel = 1;
        }
        else if (colorLevelNew == 2)
        {
            while (newColor != maxColor)
            {
                newColor = Color.Lerp(maxColor, Color.red, lerpSpeed);
                maxColor = newColor;
            }
            colorLevel = 2;
        }
        else if (colorLevelNew == 3)
        {
            while (newColor != maxColor)
            {
                newColor = Color.Lerp(maxColor, Color.yellow, lerpSpeed);
                maxColor = newColor;
            }
            colorLevel = 3;
        }
    }
}
