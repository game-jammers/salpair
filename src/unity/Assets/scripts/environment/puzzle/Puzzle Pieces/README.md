# Instructions
* Attach or copy the ColorContainer script to the player controller object

* Then either use the teleporter pre-fab or attach the ColorAccessor script to your puzzle object.

* Drag find the end target of the teleport and drag that object into the inspector.

* Fields in the accessor script are commented off and can be toggled on for different effects, such as playing a door opening animation via the mechanim.

  * Or as a button that requires the player to be moving in a downward direction to press.
