//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class BackgroundTile
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
        }
    }
}
