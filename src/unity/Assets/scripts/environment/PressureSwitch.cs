//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Salpair
{
    public class PressureSwitch
        : MonoBehaviour
        , ITrigger
    {
        // types ///////////////////////////////////////////////////////////////
        public enum TriggerType
        {
            Stand,
            JumpLight,
            JumpHeavy,
            Crash
        }

        // members /////////////////////////////////////////////////////////////
        public TriggerType triggerType                      = TriggerType.Stand;
        public GameObject[] triggerTargets                     = null;

        // public methods //////////////////////////////////////////////////////
        public void Trigger()
        {
            if( triggerTargets != null )
            {
                foreach( GameObject go in triggerTargets )
                {
                    DebugUtility.Log( "Triggered" );
                    ITriggerable triggerable = go.GetComponent<ITriggerable>();
                    if( triggerable != null )
                    {
                        triggerable.OnTrigger( this );
                    }
                    else
                    {
                        go.SendMessage( "OnTrigger", this );
                    }
                }
            }
        }

        protected virtual void OnTriggerEnter( Collider other )
        {
            Character character = other.GetComponent<Character>();
            if( character == null ) return;

            switch( triggerType )
            {
                case TriggerType.Stand: Trigger(); break;
                case TriggerType.JumpLight: {
                    if( character.datamodel.isGrounded == false && character.datamodel.isJumping == false )
                    {
                        Trigger();
                    }
                }
                break;
                case TriggerType.JumpHeavy: {
                    if( character.datamodel.isGrounded == false
                        && character.datamodel.isJumping == false
                        && character.datamodel.isGrounded.elapsed > character.datamodel.stats.movement.timeToHeavyLand )
                    {
                        Trigger();
                    }
                }
                break;
                case TriggerType.Crash: {
                    if( character.datamodel.isGrounded == false
                        && character.datamodel.isJumping == false
                        && character.datamodel.isGrounded.elapsed > character.datamodel.stats.movement.timeToCrashLand )
                    {
                        Trigger();
                    }
                }
                break;
            }
        }
    }
}
