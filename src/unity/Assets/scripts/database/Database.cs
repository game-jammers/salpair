//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Salpair
{
	public static class Database
	{
		// members ////////////////////////////////////////////////////////////
		public static AudioDb audio;

        private static bool initialized                     = false;

		// constructor / initializer //////////////////////////////////////////
		static Database()
		{
			Initialize();
		}

        public static void Initialize()
        {
            if( initialized == false )
            {
                audio = new AudioDb();
            }
        }

		// public methods /////////////////////////////////////////////////////
		public static T[] LoadAllDataFromJson<T>( string dir )
			where T: IJsonSerializable, new()
		{
			List<T> result = new List<T>();
			TextAsset[] textAssets = Resources.LoadAll<TextAsset>( dir );
			foreach( TextAsset text in textAssets )
			{
				JsonObject json = JsonObject.FromString( text.text );
				T[] dataArray = json.GetField<T[]>("entries");
				result.AddRange( dataArray );
			}

			return result.ToArray();
		}

		public static string FormatCurrency( int currency )
		{
			return System.String.Format( "${0}", currency );
		}
	}
}
