//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles.Salpair
{
	public class AudioDb
	{
		// members ////////////////////////////////////////////////////////////
		private Dictionary<string,List<AudioClip>> clips    = null;

		// constructor / initializer //////////////////////////////////////////
		public AudioDb()
		{
            clips = new Dictionary<string, List<AudioClip>>();

			AudioClip[] loadedClips = Resources.LoadAll<AudioClip>( "audio/fx" );
            foreach( AudioClip clip in loadedClips )
            {
                string groupName = clip.name.Split('.')[0];
                List<AudioClip> clipGroup = null;
                if( clips.TryGetValue( groupName, out clipGroup ) == false )
                {
                    clipGroup = new List<AudioClip>();
                }
                clipGroup.Add( clip );
                clips[groupName] = clipGroup;
            }
		}

        public AudioClip GetRandomClip( string name )
        {
            List<AudioClip> findClips = null;
            if( clips.TryGetValue( name, out findClips ) )
            {
                return findClips.Random();
            }

            return null;
        }

        public void PlayRandomClip( string name, Vector3 pos, float volume )
        {
            AudioClip clip = GetRandomClip( name );
            if( clip != null )
            {
                AudioSource.PlayClipAtPoint( clip, pos, volume );
            }
        }
	}
}
