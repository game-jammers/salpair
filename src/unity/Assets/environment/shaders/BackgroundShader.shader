 // unlit, vertex color, alpha blended, offset uv's
 // cull off

 Shader "BlendVertexColorWithUV"
 {
     Properties
     {
         _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     }

     SubShader
     {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend SrcAlpha OneMinusSrcAlpha
         LOD 110

         Pass
         {
              CGPROGRAM
              #pragma vertex vert
              #pragma fragment frag
              #define UNITY_PASS_FORWARDBASE
              #include "UnityCG.cginc"
              #pragma multi_compile_fwdbase_fullshadows
              #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2
              #pragma target 3.0

              uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
              uniform float _Distance;

              struct VertexInput {
                  float4 vertex : POSITION;
                  float2 texcoord0 : TEXCOORD0;
              };

              struct VertexOutput {
                  float4 pos : SV_POSITION;
                  float2 uv0 : TEXCOORD0;
              };

              VertexOutput vert (VertexInput v) {
                  VertexOutput o = (VertexOutput)0;
                  o.uv0 = v.texcoord0;
                  o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                  return o;
              }

              float4 frag(VertexOutput i) : COLOR {
                  ////// Lighting:
                  ////// Emissive:
                  float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                  float3 emissive = lerp(_MainTex_var.rgb,dot(_MainTex_var.rgba,float3(0.3,0.59,0.11)),_Distance);
                  float3 finalColor = emissive;
                  return fixed4(finalColor,1);
              }
              ENDCG
         }
     }

     SubShader
     {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off Fog { Mode Off }
         LOD 100

         BindChannels
         {
             Bind "Vertex", vertex
             Bind "TexCoord", texcoord
             Bind "Color", color
         }

         Pass
         {
             Lighting Off
             SetTexture [_MainTex] { combine texture * primary }
         }
     }
 }
