 // unlit, vertex color, alpha blended, offset uv's
 // cull off

 Shader "blacktriangles/background"
 {
     Properties
     {
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
        _Distance ("Distance", Range(0, 1)) = 0
     }

     SubShader
     {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend SrcAlpha OneMinusSrcAlpha
         LOD 110

         Pass
         {
             CGPROGRAM
             #pragma vertex vert_vct
             #pragma fragment frag_mult
             #pragma fragmentoption ARB_precision_hint_fastest
             #include "UnityCG.cginc"

             sampler2D _MainTex;
             float4 _MainTex_ST;
             uniform float _Distance;

             struct vin_vct
             {
                 float4 vertex : POSITION;
                 float4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
             };

             struct v2f_vct
             {
                 float4 vertex : POSITION;
                 fixed4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
             };

             v2f_vct vert_vct(vin_vct v)
             {
                 v2f_vct o;
                 o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                 o.color = v.color;
                 o.texcoord = TRANSFORM_TEX (v.texcoord, _MainTex);;
                 return o;
             }

             fixed4 frag_mult(v2f_vct i) : COLOR
             {
                fixed4 col = tex2D(_MainTex, i.texcoord) * i.color;
                col = fixed4( col.a, 0, 0, 1 );
                //col = lerp(col.rgba,dot(col.rgba,float3(0.3,0.59,0.11)),_Distance);
                return col;
             }

             ENDCG
         }
     }
 }
