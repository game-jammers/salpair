//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
	public class SocketServer
	{
		// constants ///////////////////////////////////////////////////////////
		private const int kSocketBufferSize                 = 512;

        // events /////////////////////////////////////////////////////////////////
        public delegate void ClientConnectedCallback( ServerConnection conn );
        public delegate void ClientDisconnectedCallback( ServerConnection conn );

        public event ClientConnectedCallback OnClientConnected;
        public event ClientDisconnectedCallback OnClientDisconnected;

		// members /////////////////////////////////////////////////////////////
        public IList<ServerConnection> activeConnections    { get { return connections.AsReadOnly(); } }
        public bool isReady                                 { get; private set; }
        private SocketServerConfig config                   = SocketServerConfig.Default();
		private Socket listenSocket                         = null;
		private List<ServerConnection> connections          = new List<ServerConnection>();

        // constructor / destructor ////////////////////////////////////////////
        public SocketServer()
        {
            isReady = false;
        }

        ~SocketServer()
        {
            Stop();
        }

		// public methods //////////////////////////////////////////////////////
		public bool Start( SocketServerConfig _config )
		{
            DebugUtility.Assert( _config.bufferSize > 0, "SocketServer initailized with a 0 sized buffer! Using default of 512" );
            DebugUtility.Assert( _config.connConfig.bufferSize > 0, "SocketServer initialized with a connection buffer size of 0!  Using default of 512" );
            _config.bufferSize = 512;
            _config.connConfig.bufferSize = 512;

            config = _config;
			bool result = true;
			try
			{
				listenSocket = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
				IPEndPoint endpoint = new IPEndPoint( IPAddress.Any, config.port );
				listenSocket.Bind( endpoint );
				listenSocket.Listen( config.bufferSize );
				listenSocket.BeginAccept( new AsyncCallback( OnClientConnect ), null );
			}
			catch( SocketException ex )
			{
				DebugUtility.Error( "Failed to bind socket: " + ex.Message );
				result = false;
			}

            isReady = result;

			return result;
		}

		public List<RawPacket> TakePackets()
		{
			List<RawPacket> result = new List<RawPacket>();
            lock (connections)
            {
                foreach (ServerConnection conn in connections)
                {
                    result.AddRange(conn.TakePackets());
                }
            }
			return result;
		}

		public void Stop()
		{
			foreach( ServerConnection conn in connections )
			{
				conn.DoDisconnect();
			}

			connections.Clear();
            listenSocket.Close();
		}

		public void Disconnect( ServerConnection conn )
		{
            SignalClientDisconnected( conn );
			conn.DoDisconnect();
			connections.Remove( conn );
		}

		public void Disconnect( Guid guid )
		{
			ServerConnection conn = connections.Find( (test)=>{ return test.guid == guid; } );
			if( conn != null )
			{
				Disconnect(conn);
			}
		}

		// private methods /////////////////////////////////////////////////////
        private void SignalClientConnected( ServerConnection conn )
        {
            if( OnClientConnected != null )
            {
                OnClientConnected( conn );
            }
        }

        private void SignalClientDisconnected( ServerConnection conn )
        {
            if( OnClientDisconnected != null )
            {
                OnClientDisconnected( conn );
            }
        }

		private void OnClientConnect( IAsyncResult asyn )
		{
			try
			{
                ServerConnection conn = new ServerConnection( this, listenSocket.EndAccept(asyn), config.connConfig );
                lock (connections)
                {
                    connections.Add(conn);
                }
                listenSocket.BeginAccept( new AsyncCallback( OnClientConnect ), null );
                SignalClientConnected( conn );
			}
			catch( System.ObjectDisposedException )
			{
                // this occurs naturally as a part of shutdown
				//DebugUtility.Error( "Failed to connect to client: " + ex.ToString() );
			}
			catch( SocketException ex )
			{
				DebugUtility.Error( "Failed to connect to client: " + ex.Message );
			}
		}
	}
}
