
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
	public class NetworkError
	{
		// members ////////////////////////////////////////////////////////////
		public BaseConnection connection				    { get; private set; }
		public string message							    { get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public NetworkError( BaseConnection _connection, string msg )
		{
			connection = _connection;
			message = msg;
		}
	}
}
