//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
    public class ServerConnection
        : BaseConnection
    {
        // members /////////////////////////////////////////////////////////////
        public SocketServer server                          { get; private set; }
        public Guid guid                                    { get; private set; }
        private Socket socket                               = null;
        private ServerConnectionConfig config               = ServerConnectionConfig.Default();
        private byte[] dataBuffer                           = null;
        private RawPacketCreator packetCreator              = new RawPacketCreator();

        private Thread sendPacketThread                     = null;
        private Thread receivePacketThread                  = null;
        private bool shutdown                               = false;

        private List<RawPacket> incomingPackets             = new List<RawPacket>();
        private List<Packet> outgoingPackets                = new List<Packet>();

        // constructor / initializer ///////////////////////////////////////////
        public ServerConnection( SocketServer _server, Socket _socket, ServerConnectionConfig _config )
        {
            DebugUtility.Assert( _config.bufferSize > 0, "ServerConnection initailized with a 0 sized buffer!" );
            server = _server;
            guid = Guid.NewGuid();
            socket = _socket;
            config = _config;
            dataBuffer = new byte[ config.bufferSize ];
            sendPacketThread = new Thread( new ThreadStart( this.SendPacketLoop ) );
            receivePacketThread = new Thread( new ThreadStart( this.ReceivePacketLoop ) );
            sendPacketThread.Start();
            receivePacketThread.Start();
        }

        // BaseConnection //////////////////////////////////////////////////////
        public override bool isConnected                    { get { return ( socket != null && socket.Connected ); } }

        // public methods /////////////////////////////////////////////////////////
        public override bool Connect( string hostAddress, int port )
        {
            throw new System.NotImplementedException( "Do not call Connect on ServerConnections!" );
        }

        public override bool Reconnect()
        {
            throw new System.NotImplementedException( "Do not call Reconnect on ServerConnections!" );
        }

        public override void Disconnect()
        {
            server.Disconnect( this );
        }

        public void DoDisconnect()
        {
            shutdown = true;
            if( sendPacketThread != null )
            {
                sendPacketThread.Join( 3000 );
                sendPacketThread = null;
            }

            if( receivePacketThread != null )
            {
                receivePacketThread.Join( 3000 );
                receivePacketThread = null;
            }

            socket.Close();
        }

        public override void SendPacket( Packet packet )
        {
            if( packet == null ) return;

            lock( outgoingPackets )
            {
                outgoingPackets.Add( packet );
            }
        }

        public override void SendPackets( IEnumerable<Packet> packets )
        {
            if( packets == null ) return;

            lock( outgoingPackets )
            {
                outgoingPackets.AddRange( packets );
            }
        }

        public override List<RawPacket> TakePackets()
        {
            List<RawPacket> result = null;
            lock( incomingPackets )
            {
              result = new List<RawPacket>( incomingPackets );
              incomingPackets.Clear();
            }

            return result;
        }

        // public methods //////////////////////////////////////////////////////
        private void SendPacketLoop()
        {
            while( !shutdown )
            {
                List<Packet> sendPackets = null;
                lock( outgoingPackets )
                {
                    if( outgoingPackets.Count > 0 )
                    {
                        sendPackets = new List<Packet>(outgoingPackets);
                        outgoingPackets.Clear();
                    }
                }

                if( sendPackets != null )
                {
                    foreach( Packet packet in sendPackets )
                    {
                        RawPacket raw = packet.Encode();
                        socket.Send( raw.ToBytes() );
                    }
                }
            }
        }

        private void ReceivePacketLoop()
        {
            while( !shutdown )
            {
                int readCount = 0;
                try
                {
                    readCount = socket.Receive( dataBuffer );
                }
                catch( System.Exception ex )
                {
                    Disconnect();
                    NotifyError( ex.Message );
                }

                if( readCount > 0 )
                {
                    MemoryStream stream = new MemoryStream( readCount );
                    stream.Write( dataBuffer, 0, readCount );
                    stream.Seek( 0, SeekOrigin.Begin );

                    BinaryReader reader = new BinaryReader( stream );
                    RawPacket packet = packetCreator.Read( reader );

                    if( packet != null )
                    {

                        packet.source = this;
                        lock( incomingPackets )
                        {
                            incomingPackets.Add( packet );
                        }
                    }
                }
            }
        }
    }
}
