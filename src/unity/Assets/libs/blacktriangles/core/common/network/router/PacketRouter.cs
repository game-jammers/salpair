//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace blacktriangles.Network
{
	public class PacketRouter
	{
		// members ////////////////////////////////////////////////////////////
		private Dictionary<int,System.Type> packetTypes			= null;
		private Dictionary<int,System.Delegate> packetHandlers 	= null;
		public bool isInitialized								{ get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public PacketRouter()
		{
			isInitialized = false;
			Initialize();
		}

        public PacketRouter( Assembly[] assemblies )
        {
            isInitialized = false;
            Initialize( assemblies );
        }

		public void Initialize()
		{
			Initialize( new Assembly[] { Assembly.GetExecutingAssembly() } );
		}

        public void Initialize( System.Reflection.Assembly[] assemblies )
        {
            if( !isInitialized )
			{
				packetTypes = new Dictionary<int,System.Type>();
				packetHandlers = new Dictionary<int,System.Delegate>();

                foreach( Assembly asm in assemblies )
                {
				    System.Type[] packetTypeList = AssemblyUtility.CollectTypesWithAttribute<PacketAttribute>( asm );
				    foreach( System.Type type in packetTypeList )
				    {
					    PacketAttribute packetAttribute = AssemblyUtility.GetAttribute<PacketAttribute>( type );
					    packetTypes[ packetAttribute.id ] = type;
				    }
                }

				isInitialized = true;
			}
        }

		// public methods /////////////////////////////////////////////////////
		public bool Route( RawPacket rawPacket )
		{
			bool result = false;

			System.Type type = default(System.Type);
			if( packetTypes.TryGetValue( rawPacket.header.id, out type ) )
			{
				Packet packet = System.Activator.CreateInstance( type ) as Packet;
				if( packet != null )
				{
					packet.Decode( rawPacket );
					result = Route( rawPacket.source, packet );
				}
			}

			return result;
		}

		public void AddRoute<PacketType>( System.Func<BaseConnection,PacketType,bool> handler )
			where PacketType: Packet
		{
			PacketAttribute packetAttribute = AssemblyUtility.GetAttribute<PacketAttribute>( typeof(PacketType) );
			if( packetAttribute != null )
			{
				int id = packetAttribute.id;
				System.Delegate result = null;
				if( packetHandlers.TryGetValue( id, out result ) )
				{
					System.Func<BaseConnection, PacketType, bool> castHandlers = (System.Func<BaseConnection, PacketType,bool>)result;
					castHandlers -= handler;
					castHandlers += handler;
					result = (System.Delegate)castHandlers;
				}
				else
				{
					result = (System.Delegate)handler;
				}

				packetHandlers[id] = result;
			}
		}

        public void RemoveRoute<PacketType>( System.Func<BaseConnection,PacketType,bool> handler )
        {
            PacketAttribute packetAttribute = AssemblyUtility.GetAttribute<PacketAttribute>( typeof(PacketType) );
			if( packetAttribute != null )
			{
				int id = packetAttribute.id;
				System.Delegate result = null;
				if( packetHandlers.TryGetValue( id, out result ) )
				{
					System.Func<BaseConnection, PacketType,bool> castHandlers = (System.Func<BaseConnection, PacketType,bool>)result;
					castHandlers -= handler;
					result = (System.Delegate)castHandlers;
				}
				else
				{
					result = (System.Delegate)handler;
				}

				packetHandlers[id] = result;
			}
        }

		// private methods ////////////////////////////////////////////////////
		private bool Route( BaseConnection connection, Packet packet )
		{
			bool result = false;

			System.Delegate handler = null;
			if( packetHandlers.TryGetValue( packet.id, out handler ) )
			{
                if( handler != null )
                {
				    result = (bool)handler.DynamicInvoke( connection, packet );
                }
			}

			return result;
		}
    }
}
