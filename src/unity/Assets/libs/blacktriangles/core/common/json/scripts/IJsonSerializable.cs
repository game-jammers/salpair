//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
    public interface IJsonSerializable
    {
        JsonObject ToJson();
        void FromJson( JsonObject json );
    }
}
