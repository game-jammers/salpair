//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;

namespace blacktriangles
{
	public abstract class Coroutine
	{
		// members ////////////////////////////////////////////////////////
		private IEnumerator enumerator							= null;
		private bool started 									= false;
		private bool completed									= false;

		// public methods /////////////////////////////////////////////////
		public virtual void Start()
		{
			started = true;
			enumerator = OnUpdate();
		}

		public virtual bool Update()
		{
			if( started == false ) Start();
			bool result = Process( enumerator );

			if( result == false && !completed )
			{
				completed = true;
				OnComplete();
			}

			return result;
		}

		// protected methods //////////////////////////////////////////////
		protected abstract IEnumerator OnUpdate();
		protected virtual void OnComplete()						{}

		protected IEnumerator WaitForSeconds( double seconds )
		{
			System.DateTime startedWaiting = System.DateTime.UtcNow;
			while( ( System.DateTime.UtcNow - startedWaiting ).TotalSeconds < seconds )
			{
				yield return true;
			}
		}

		// private methods ////////////////////////////////////////////////
		private bool Process( IEnumerator enumerator )
		{
			bool result = false;
			if( enumerator != null )
			{
				IEnumerator subEnumerator = enumerator.Current as IEnumerator;
				if( subEnumerator != null )
				{
					result = Process( subEnumerator );
					if( !result )
					{
						result = enumerator.MoveNext();
					}
				}
				else
				{
					result = enumerator.MoveNext();
				}
			}

			return result;
		}
	}
}
