//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using MiscUtil;

namespace blacktriangles
{
	[System.Serializable]
	public class Range<T>
	{
		// members ////////////////////////////////////////////////////////////
		public T min;
		public T max;
		public T length											{ get { return Operator.Subtract( max, min ); } }

		// constructor / initializers /////////////////////////////////////////
		public Range()
			: this( default(T), default(T) )
		{
		}

		public Range( T _min, T _max )
		{
			min = _min;
			max = _max;
		}

		// public methods /////////////////////////////////////////////////////////
		public T Clamp( T val )
		{
			T result = Operator.LessThan( val, min ) ? min : val;
			result = Operator.GreaterThan( result, max ) ? max : result;
			return result;
		}

		public bool IsInRange( T val )
		{
			return Operator.GreaterThan( val, min ) && Operator.LessThan( val, max );
		}
	}

	[System.Serializable]
	public class IntRange
		: Range<int>
	{
	public System.Random rng									= null;

		public IntRange( int min, int max )
				: base( min, max )
		{
		}

	public IntRange( int min, int max, System.Random _rng )
		: base( min, max )
	{
		rng = _rng;
	}

		public int Random()
		{
		if( rng == null )
		{
				return btRandom.Range( min, max );
		}
		else
		{
			return rng.Next( min, max );
		}
		}
	}

	[System.Serializable]
	public class FloatRange
		: Range<float>
	{
	public System.Random rng									= null;

		public FloatRange( float min, float max )
			: base( min, max )
		{
		}

	public FloatRange( float min, float max, System.Random _rng )
			: base( min, max )
		{
		rng = _rng;
		}

		public float Random()
		{
		if( rng == null )
		{
		 		return btRandom.Range( min, max );
		}
		else
		{
			return rng.NextFloat( min, max );
		}
		}
	}
}
