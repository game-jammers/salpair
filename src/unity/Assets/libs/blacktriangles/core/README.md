# UnityCore

UnityCore is a set of utilities and common scripts shared by all blacktriangles
Unity projects.

Maintained by Howard N Smith
howard@blacktriangles.com
http://blog.blacktriangles.com

Public Repository:
- ssh: git@git.blacktriangles.net:com/unitycore
- https: https://git.blacktriangles.net/com/unitycore

Credits
-------
This repository uses resources from the following sources

Icons By:
- Glyphicons Pro (http://glyphicons.com)
- Mario Del Valle (mariodelvalle.github.io/CaptainIconWeb)

JSON Parser Heavily Modified From:
- https://gist.github.com/darktable/1411710 (Calvin Rien)
- http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html (Patrick van Bergen)

Grid Texture:
- https://www.flickr.com/photos/zooboing/4302326159/ (Patrick Hoesly)
    -[https://creativecommons.org/licenses/by/2.0/]

TestCharacterSpritesheet from:
- http://opengameart.org/content/player-0 (renegreg)
    - https://www.facebook.com/The.Dark.Puppet
    -[http://creativecommons.org/licenses/by/3.0/]

`MiscUtil` by Jon Skeet:
- http://www.yoda.arachsys.com/csharp/miscutil/

`protobuf-net` net by Marc Gravell
- https://github.com/mgravell/protobuf-net

`Deque` class by Nito (Stephen Cleary)
- http://nitodeque.codeplex.com/SourceControl/latest#Source/PortableClassLibrary/Deque.cs

`BitmaskDrawer` from unity3d answers post by Bunny83
- http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html

`PasswordHash` from Roger Knapp's awesome blog post
- http://csharptest.net/470/another-example-of-how-to-store-a-salted-password-hash/
