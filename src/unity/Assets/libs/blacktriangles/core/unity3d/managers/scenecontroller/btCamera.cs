//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public abstract class btCamera
    	: MonoBehaviour
	{
		// types //////////////////////////////////////////////////////////////
	    public enum UpdateType
	    {
			Update,
			FixedUpdate,
			LateUpdate,
			Manual
	    }

	    // members ////////////////////////////////////////////////////////////
	    public Camera unityCamera								= null;
		public UpdateType updateType                            = UpdateType.Manual;
	    public float speed										= 10.0f;
        public LayerMask selectable;

		// camera callbacks ///////////////////////////////////////////////////////
	    public virtual void UpdateCamera( float bt )
	    {
	    }

	    // unity callbacks ////////////////////////////////////////////////////////
	    protected virtual void Update()
	    {
			if( updateType == UpdateType.Update )
			{
				UpdateCamera( Time.deltaTime );
			}
	    }

	    protected virtual void FixedUpdate()
	    {
			if( updateType == UpdateType.FixedUpdate )
			{
				UpdateCamera( Time.fixedDeltaTime );
			}
	    }

	    protected virtual void LateUpdate()
	    {
			if( updateType == UpdateType.LateUpdate )
			{
				UpdateCamera( Time.deltaTime );
			}
	    }

	    //// utilities ////////////////////////////////////////////////////////////
	    public Vector3 WorldToScreenPoint( Vector3 worldPos )
	    {
			return unityCamera.WorldToScreenPoint( worldPos );
	    }

        public Ray ScreenToRay( Vector3 screenPos )
        {
            return unityCamera.ScreenPointToRay( screenPos );
        }

        public Rect GetScreenBounds( Plane groundPlane )
        {

            Vector3 bottomLeft = ScreenToWorld( Vector3.zero, groundPlane );
            Vector3 topRight = ScreenToWorld( new Vector3( Screen.width, Screen.height, 0.0f ), groundPlane );
            Rect result = Rect.MinMaxRect( bottomLeft.x, bottomLeft.y, topRight.x, topRight.y );
            return result;
        }

	    public Vector3 ScreenToWorld( Vector3 screenPos, Plane groundPlane )
		{
			Vector3 result = Vector3.zero;

			if( unityCamera != null )
			{
				Ray ray = ScreenToRay( screenPos );

				float distance = 0f;
				groundPlane.Raycast( ray, out distance );

				result = ray.GetPoint( distance );
			}

			return result;
		}

		public Vector3 ScreenToWorld( Vector3 screenPos )
		{
			Plane groundPlane = new Plane( Vector3.forward, Vector3.zero );
			return ScreenToWorld( screenPos, groundPlane );
		}

        public bool MousePick( Vector3 screenPos, out RaycastHit result, float distance = Mathf.Infinity )
        {
            return MousePick( screenPos, selectable, distance, out result );
        }

	    public bool MousePick( Vector3 screenPos, LayerMask mask, float distance, out RaycastHit result )
	    {
            result = new RaycastHit();
			bool success = false;
			if( unityCamera != null )
			{
				Ray ray = unityCamera.ScreenPointToRay( screenPos );
				success = Physics.Raycast( ray.origin, ray.direction, out result, distance, mask );
			}

			return success;
		}

        public ComponentType MousePick<ComponentType>( Vector3 screenPos, float distance = Mathf.Infinity )
            where ComponentType: MonoBehaviour
        {
            ComponentType result = null;
            RaycastHit hit;
            if( MousePick( screenPos, out hit, distance ) )
            {
                result = hit.collider.gameObject.GetComponent<ComponentType>();
            }
            return result;
        }
	}
}
