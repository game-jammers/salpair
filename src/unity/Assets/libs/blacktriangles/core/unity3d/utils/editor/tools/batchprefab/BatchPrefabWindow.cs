//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

namespace blacktriangles
{
	public class BatchPrefabWindow
		: EditorWindow
	{
		// members //////////////////////////////////////////////////////////////
		private const string kTitle 	                              = "Batch Prefab Creator";
        private const string kMenuPath 	                            = "Tools/blacktriangles/Common/Batch Prefab Creator Window";

        private List<GameObject> gos                                = new List<GameObject>();
        private Vector2 scrollPos                                   = Vector2.zero;

		// constructor / destructor ///////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			BatchPrefabWindow window = GetWindow<BatchPrefabWindow>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
            gos = new List<GameObject>();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
            EditorGUIUtility.OnGUI();
			GUILayout.BeginVertical( GUI.skin.box );
				scrollPos = GUILayout.BeginScrollView( scrollPos );
    				EditorGUIUtility.QuickListField<GameObject>(
                            "GameObjects",
                            gos,
                            (g)=>{ return EditorGUIUtility.ThinObjectField( System.String.Empty, g, true); },
                            EditorGUIUtility.GetNullClass<GameObject>,
                            GUIStyles.buttonNormal
                        );
				GUILayout.EndScrollView();

      			if( GUILayout.Button( "Get From Selection" ) )
      			{
      				gos.Clear();
      				foreach( GameObject selection in Selection.objects )
      				{
      					if( selection != null )
      					{
      						gos.Add( selection );
      					}
      				}
      			}

            GUILayout.EndVertical();

			if( GUILayout.Button( "Create" ) )
			{
				CreatePrefabs( gos.ToArray() );
			}
        }

        private void CreatePrefabs( GameObject[] gameObjects )
        {
            string prefabDir = EditorUtility.SaveFolderPanel( kTitle, "Assets/", System.String.Empty );
            if( prefabDir.Length > 0 )
            {
                prefabDir = FileUtility.MakePathRelativeToAssetDir( prefabDir );

                float current = 0f;
                foreach( GameObject go in gameObjects )
                {
                    EditorUtility.DisplayProgressBar( System.String.Format( "Creating Prefab {0}/{1}", current, gameObjects.Length ), go.name, (float)current++/(float)gameObjects.Length );

                    string path = System.IO.Path.Combine( prefabDir, go.name + ".prefab" );
                    Object prefab = PrefabUtility.CreateEmptyPrefab( path );
                    PrefabUtility.ReplacePrefab( go, prefab );
                }

                EditorUtility.ClearProgressBar();
            }

            AssetDatabase.Refresh();
        }
	}
}
