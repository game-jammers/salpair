//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

namespace blacktriangles
{
	public class ExportToAFrame
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////////
		private const string kTitle 	                    = "Export AFrame";
        private const string kMenuPath 	                    = "Tools/blacktriangles/Export/Export Scene To AFrame";
        private const string kHeader = "<!DOCTYPE html>\n"
                                     + "<html>\n"
                                     + "\t<head>\n"
                                     + "\t\t<meta charset=\"utf-8\">\n"
                                     + "\t\t<title>Hello, World! - A-Frame</title>\n"
                                     + "\t\t<meta name=\"description\" content=\"Shader Test\">\n"
                                     + "\t\t<script src=\"aframe.js\"></script>\n"
									 + "\t\t<script src=\"shaders.js\"></script>\n"
                                     + "\t</head>\n"
                                     + "\t<body>\n"
                                     + "\t\t<a-scene>\n"
									 + "\t\t\t<a-sky color=\"#ECECEC\"></a-sky>\n"
									 + "\t\t\t<a-entity light=\"type: point; intensity: 0.75; distance: 50; decay: 2\" position=\"0 10 10\"></a-entity>\n\n"
			;

        private const string kFooter = "\t\t</a-scene>\n"
                                     + "\t</body>\n"
                                     + "</html>";

		private const string kMaterialDefault = "material=\"shader: custom; TopColor: 0.66 0.13 0.13; SideColor: 0.2 0.12 0.4; BottomColor: 0 0 0; Banding: 3.8\"";

		// members /////////////////////////////////////////////////////////////
		private string outPath								= System.String.Empty;
		private string materialAddon						= kMaterialDefault;

		// constructor / destructor ///////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			ExportToAFrame window = GetWindow<ExportToAFrame>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		// unity callbacks /////////////////////////////////////////////////////
		private void Initialize()
		{
            outPath = System.String.Empty;
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			GUILayout.BeginHorizontal();
				outPath = EditorGUILayout.TextField( "Out Path", outPath );
				if( GUILayout.Button( "...", GUILayout.Width( 25 ) ) )
				{
					PickOutputPath();
				}
			GUILayout.EndHorizontal();
			materialAddon = EditorGUILayout.TextField( "Material", materialAddon );
			if( GUILayout.Button( "Export" ) )
			{
				ExportScene();
			}
		}

        // private methods /////////////////////////////////////////////////////
		private void PickOutputPath()
		{
			outPath = EditorUtility.SaveFilePanel( "Save AFrame Scene", "", "index.html", "html" );
		}

		private void ExportScene()
		{
            if( outPath.Length <= 0 )
            {
				PickOutputPath();
			}

			if( outPath.Length > 0 )
			{
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append( kHeader );

                Transform[] allTransforms = UnityEngine.Object.FindObjectsOfType<Transform>();
                foreach( Transform obj in allTransforms )
                {
					MeshFilter filter = obj.GetComponent<MeshFilter>();
					if( filter != null && filter.sharedMesh.name == "Cube" )
                    {
                        AddCube( sb, obj );
                    }

					Light light = obj.GetComponent<Light>();
					if( light != null )
					{
						AddLight( sb, light );
					}
                }

                sb.Append( kFooter );

                System.IO.File.WriteAllText( outPath, sb.ToString() );
            }
		}

		private string GetTransformString( Transform obj )
		{
			return System.String.Format( "position=\"{0} {1} {2}\" rotation=\"{3} {4} {5}\" width=\"{6}\" height=\"{7}\" depth=\"{8}\""
                    , obj.position.x
                    , obj.position.y
                    , obj.position.z
                    , obj.rotation.eulerAngles.x
                    , obj.rotation.eulerAngles.y
                    , obj.rotation.eulerAngles.z
                    , obj.localScale.x
                    , obj.localScale.y
                    , obj.localScale.z
                );
		}

		private string GetColorString( Color color )
		{
			int r = (int)(255 * color.r);
			int g = (int)(255 * color.g);
			int b = (int)(255 * color.b);

			return System.String.Format( "#{0}{1}{2}", r.ToString("X"), g.ToString("X"), b.ToString("X") );
		}

        private void AddCube( System.Text.StringBuilder sb, Transform obj )
        {
			sb.Append( System.String.Format( "\t\t\t<!-- CUBE: {0} -->\n", obj.name ) );
            sb.Append( System.String.Format( "\t\t\t<a-box {0} {1}></a-box>\n\n", GetTransformString( obj ), materialAddon ) );
        }

		private void AddLight( System.Text.StringBuilder sb, Light light )
		{
			sb.Append( System.String.Format( "\t\t\t<!-- LIGHT: {0} -->\n", light.name ) );
			sb.Append( System.String.Format( "\t\t\t<a-entity {0} light=\"color:{1}; intensity:{2}; "
					, GetTransformString(light.transform)
					, GetColorString(light.color)
					, light.intensity
				));
			switch( light.type )
			{
				case LightType.Spot: {
					sb.Append( System.String.Format("type: spot; angle: {0}; distance: {1}; target: null;"
							, light.spotAngle
							, light.range
						));
				}
				break;

				case LightType.Directional: {
					sb.Append( "type: directional;" );
				}
				break;

				case LightType.Point: {
					sb.Append( System.String.Format( "type: point; distance:{0};", light.range ));
				}
				break;
			}
			sb.Append( "\"></a-entity>\n" );
		}
    }
}
