//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class RandomizePosition
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public Vector3 min										= Vector3.zero;
		public Vector3 max										= Vector3.zero;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			transform.localPosition += Vector3Extension.RandomRange( min, max );
		}
	}
}
