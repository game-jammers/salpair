//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class DestroyOnCollide
		: MonoBehaviour
	{
        // events /////////////////////////////////////////////////////////////
        public delegate void DestroyedCallback( DestroyOnCollide obj, GameObject other );
        public event DestroyedCallback OnDestroyed;

		// members ////////////////////////////////////////////////////////////
		public bool onTriggers									= false;
		public LayerMask ignoreLayers						    = default(LayerMask);
        public float delay                                      = 0.0f;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnCollisionEnter( Collision collision )
		{
			CheckDestroy( collision.gameObject );
		}

		protected virtual void OnTriggerEnter( Collider collider )
		{
			if( onTriggers )
			{
				CheckDestroy( collider.gameObject );
			}
		}

		protected virtual void OnCollisionEner2D( Collision2D collision )
		{
			CheckDestroy( collision.gameObject );
		}

		protected virtual void OnTriggerEnter2D( Collider2D collider )
		{
			if( onTriggers )
			{
				CheckDestroy( collider.gameObject );
			}
		}

        // private methods ////////////////////////////////////////////////////
		private void CheckDestroy( GameObject other )
		{
			if( ((1 << other.layer) & ignoreLayers.value ) == 0 )
			{
                StartCoroutine( DoDestroy( delay, other ) );
			}
		}

        private IEnumerator DoDestroy( float delay, GameObject other )
        {
            yield return new WaitForSeconds( delay );
            Destroy( gameObject );
            if( OnDestroyed != null )
            {
                OnDestroyed( this, other );
            }
        }
	}
}
