//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class ProceduralMotion
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public float rotationSpeed                          = 1.0f;
        public Vector3 rotationAxis                         = Vector3.up;

        public float bobSpeed                               = 1.0f;
        public float bobIntensity                           = 1.0f;
        public Vector3 bobAxis                              = Vector3.up;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Update()
		{
			transform.position += bobAxis.normalized * Mathf.Sin(Time.time*bobSpeed) * bobIntensity;
            transform.RotateAround( transform.position, rotationAxis, Time.deltaTime * rotationSpeed );
		}
	}
}
