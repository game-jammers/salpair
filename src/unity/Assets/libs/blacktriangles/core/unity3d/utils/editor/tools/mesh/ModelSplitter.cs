//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

namespace blacktriangles
{
	public class ModelSplitter
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath						                	= "Tools/blacktriangles/Mesh/Model Prefab Splitter";
		private const string kTitle								                  = "Model Splitter";

		private GameObject model								                    = null;
		private GameObject rigRoot								                  = null;
		private List<GameObject> ignoreDelete					              = null;
		private List<GameObject> ignoreKeep						              = null;

		// constructor / initializer //////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			ModelSplitter window = GetWindow<ModelSplitter>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			model = null;
			rigRoot = null;
			ignoreDelete = new List<GameObject>();
			ignoreKeep = new List<GameObject>();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			EditorGUIUtility.OnGUI();

			model = EditorGUILayout.ObjectField( "Model To Split", model, typeof( GameObject ), true ) as GameObject;
      rigRoot = EditorGUILayout.ObjectField( "Rig Root", rigRoot, typeof( GameObject ), true ) as GameObject;
			if( model == null || rigRoot == null )
			{
				EditorGUILayout.HelpBox( "Set a model to split, or add a rigRoot to multisplit.  RigRoots name must match in all multisplit models", MessageType.Info );
			}
			else
			{
        PrefabType prefabType = PrefabUtility.GetPrefabType( model );
        if( prefabType != PrefabType.ModelPrefabInstance && prefabType != PrefabType.PrefabInstance )
        {
          EditorGUILayout.HelpBox( "Prefab must be a ModelPrefabInstance or a PrefabInstance, it is a " + prefabType.ToString(), MessageType.Error );
        }
        else
        {
  				System.Func<GameObject,GameObject> guiCallback = (go)=>{ return EditorGUILayout.ObjectField( go, typeof(GameObject), true) as GameObject; };
  				EditorGUIUtility.QuickListField<GameObject>( "Ignore Delete", ignoreDelete, guiCallback, EditorGUIUtility.GetNullClass<GameObject>, GUIStyles.buttonNormal );
  				EditorGUIUtility.QuickListField<GameObject>( "Ignore Keep", ignoreKeep, guiCallback, EditorGUIUtility.GetNullClass<GameObject>, GUIStyles.buttonNormal );

  				if( GUILayout.Button( "Split" ) )
  				{
            GameObject split = new GameObject();
            split.name = "SPLIT";
  					Split( split, model, rigRoot.name );
            EditorUtility.ClearProgressBar();
  				}
        }
			}

      if( rigRoot != null && GUILayout.Button( "Multi Split" ) )
      {
        MultiSplit();
        EditorUtility.ClearProgressBar();
      }

		}

    private void Split( GameObject parent, GameObject splitee, string rigRootName )
    {
      GameObject split = parent;

      int count = 0;
      List<GameObject> destroy = new List<GameObject>();
      foreach( Transform child in splitee.transform )
      {
        EditorUtility.DisplayProgressBar( "Splitting Model", child.name, count++/(float)splitee.transform.childCount );
        if( child.gameObject.name != rigRootName )
        {
          // clone the original
          GameObject newGo = GameObject.Instantiate( splitee, Vector3.zero, Quaternion.identity ) as GameObject;
          newGo.name = child.name;
          newGo.transform.SetParent( split.transform );

          // mark objects for destroy
          foreach( Transform newChild in newGo.transform )
          {
            if( newChild.name != rigRootName && newChild.name != newGo.name )
            {
              destroy.Add( newChild.gameObject );
            }
          }
        }
      }

      // destroy objects
      for( int i = 0; i < destroy.Count; ++i )
      {
        GameObject.DestroyImmediate( destroy[i] );
      }

      destroy = null;
    }

    private void MultiSplit()
    {
      GameObject split = new GameObject();
      split.name = "SPLIT";

      List<GameObject> gos = new List<GameObject>( Selection.GetFiltered( typeof( GameObject ), SelectionMode.TopLevel | SelectionMode.Editable | SelectionMode.ExcludePrefab ).ConvertAll<GameObject,Object>() );
      foreach( GameObject go in gos )
      {
        Split( split, go, rigRoot.name );
      }
    }

	}
}
