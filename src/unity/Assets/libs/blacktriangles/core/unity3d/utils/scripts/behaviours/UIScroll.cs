//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class UIScroll
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		[SerializeField] UIElement element                  = null;
        [SerializeField] float scrollSpeed                  = 1.0f;
        [SerializeField] float delay                        = 5.0f;

        private float elapsed                               = 0;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			element = GetComponent<UIElement>();
		}

		protected virtual void Update()
		{
            if( elapsed < delay )
            {
                elapsed += Time.deltaTime;
            }
            else
            {
                element.rectTransform.anchoredPosition += new Vector2(0.0f, 1.0f) * scrollSpeed * Time.deltaTime;
            }
		}
	}
}
