//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class MeshUtility
	{
		// types //////////////////////////////////////////////////////////////
		public struct MergeResult
		{
			public GameObject root;
			public SkinnedMeshRenderer skin;
			public Mesh mesh;
		};

		public struct MergeResultEX
		{
			public GameObject root;
			public SkinnedMeshRenderer skin;
			public Mesh mesh;
			public SkinnedMeshRenderer[] originalSkins;
		};

		// public methods /////////////////////////////////////////////////////
		public static MergeResult MergeSkinnedMeshes( GameObject rig, SkinnedMeshRenderer[] skins )
		{
			GameObject newRigObj = GameObject.Instantiate( rig, Vector3.zero, Quaternion.identity ) as GameObject;
			GameObject newSkinObj = new GameObject("NewMesh");
			SkinnedMeshRenderer r = newSkinObj.AddComponent<SkinnedMeshRenderer>() as SkinnedMeshRenderer;

			newSkinObj.transform.SetParent( newRigObj.transform, false );

			// The SkinnedMeshRenderers that will make up a character will be
      // combined into one SkinnedMeshRenderers using multiple materials.
      // This will speed up rendering the resulting character.
      List<CombineInstance> combineInstances = new List<CombineInstance>();
      List<Material> materials = new List<Material>();
      List<Transform> bones = new List<Transform>();
			List<BoneWeight> boneWeights = new List<BoneWeight>();
      Transform[] transforms = newRigObj.GetComponentsInChildren<Transform>();

      foreach (SkinnedMeshRenderer smr in skins)
      {
        materials.AddRange(smr.sharedMaterials);
        for( int sub = 0; sub < smr.sharedMesh.subMeshCount; ++sub )
        {
          CombineInstance ci = new CombineInstance();
          ci.mesh = smr.sharedMesh;
          ci.subMeshIndex = sub;
          combineInstances.Add(ci);

    			// for each submesh, we add a copy of the bones for that submesh
    			// each submesh the bones were indexed based off the first bone being
    			// zero, however, in the combined bones array, we offset them by
    			// this sections placement within the overall list. (boneIndexOffset)
    			// note that this isn't quite as bad as it may seem, since the bone
    			// list is really just a list of references to the actual transforms
    			// in the combined mesh.
    			int boneIndexOffset = bones.Count;
    			foreach( Transform boneTrans in smr.bones )
    			{
    				string bone = boneTrans.name;
    			    foreach (Transform transform in transforms)
    			    {
    			        if (transform.name != bone) continue;
    			        bones.Add(transform);
    			        break;
    			    }
    			}

    			// we take the weights and offset them so they match up to the
    			// bones we just added.  We could streamline this process by using
    			// a lookup table
    			foreach( BoneWeight weight in smr.sharedMesh.boneWeights )
    			{
    				BoneWeight w = weight;
    				w.boneIndex0 += boneIndexOffset;
    				w.boneIndex1 += boneIndexOffset;
    				w.boneIndex2 += boneIndexOffset;
    				w.boneIndex3 += boneIndexOffset;
    				boneWeights.Add( w );
    			}
        }
      }

      // Obtain and configure the SkinnedMeshRenderer attached to
      // the character base
      r.sharedMesh = new Mesh();
      r.sharedMesh.CombineMeshes(combineInstances.ToArray(), false, false);
			r.sharedMesh.boneWeights = boneWeights.ToArray();
      r.bones = bones.ToArray();
      r.materials = materials.ToArray();

			MergeResult mergeResult = new MergeResult();
			mergeResult.root = newRigObj;
			mergeResult.skin = r;
			mergeResult.mesh = r.sharedMesh;

			mergeResult.mesh.RecalculateBounds();
			mergeResult.mesh.RecalculateNormals();

			return mergeResult;
		}

		public static MergeResultEX MergeSkinnedMeshes( GameObject rig, GameObject[] objects )
		{
			List<SkinnedMeshRenderer> smrs = new List<SkinnedMeshRenderer>();
			foreach( GameObject go in objects )
			{
        if( go != null )
        {
				  smrs.AddRange( go.GetComponentsInChildren<SkinnedMeshRenderer>( go.transform ) );
        }
			}

			MergeResultEX result = new MergeResultEX();
			result.originalSkins = smrs.ToArray();
			MergeResult mergeResult = MergeSkinnedMeshes( rig, result.originalSkins );
			result.root = mergeResult.root;
			result.skin = mergeResult.skin;
			result.mesh = mergeResult.mesh;

			return result;
		}

    public static MergeResultEX MergeSkinnedMeshes( GameObject rig, Component[] components )
    {
      List<GameObject> gos = new List<GameObject>();
      foreach( Component comp in components )
      {
        if( comp != null )
        {
          gos.Add( comp.gameObject );
        }
      }

      return MergeSkinnedMeshes( rig, gos.ToArray() );
    }

    public static MergeResultEX MergeSkinnedMeshes( Component rig, Component[] components )
    {
      return MergeSkinnedMeshes( rig.gameObject, components );
    }

		public static Mesh MergeMeshes( MeshFilter[] meshes )
		{
			List<CombineInstance> combines = new List<CombineInstance>();
			foreach( MeshFilter filter in meshes )
			{
				CombineInstance ci = new CombineInstance();
				ci.mesh = filter.sharedMesh;
				ci.transform = filter.gameObject.transform.localToWorldMatrix;
				combines.Add( ci );
			}

			Mesh result = new Mesh();
			result.CombineMeshes( combines.ToArray(), true, true );
			return result;
		}
	}
}
