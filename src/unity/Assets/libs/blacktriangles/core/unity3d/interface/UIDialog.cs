//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#define NO_THIRD_PARTY

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using blacktriangles;

#if !NO_THIRD_PARTY
    using TMPro;
#endif

namespace blacktriangles
{
    public class UIDialog
        : UIElement
    {
        // events //////////////////////////////////////////////////////////////
        public delegate void ShowCallback( UIDialog dialog );
        public delegate void CloseCallback( UIDialog dialog, UIDialog.Result result );

        public event ShowCallback OnShow;
        public event CloseCallback OnClose;

        // types ///////////////////////////////////////////////////////////////
        public enum Result
        {
            Accept,
            Cancel
        }

        [System.Serializable]
        public struct Request
        {
            public int priority;
            public string title;
            public string description;
            public string accept;
            public string cancel;

            public float fadeInTime;
            public float fadeOutTime;

            public System.Action<UIDialog.Result> callback;
        }

        // members /////////////////////////////////////////////////////////////
        #if NO_THIRD_PARTY
            public Text title                               = null;
            public Text description                         = null;
            public Text accept                              = null;
            public Text cancel                              = null;
        #else
            public TextMeshProUGUI title                    = null;
            public TextMeshProUGUI description              = null;
            public TextMeshProUGUI accept                   = null;
            public TextMeshProUGUI cancel                   = null;
        #endif

        public bool isActive                                { get { return request.HasValue; } }
        public Request? request                             { get; private set; }

        // constructor / destructor ////////////////////////////////////////////
        public bool Show( Request _request )
        {
            if( isActive ) return false;

            request = _request;

            if( title != null )
            {
                title.text = request.Value.title;
            }

            if( description != null )
            {
                description.text = request.Value.description;
            }

            if( accept != null )
            {
                accept.text = request.Value.accept;
            }

            if( cancel != null )
            {
                cancel.text = request.Value.cancel;
            }

            Show( request.Value.fadeInTime );
            if( OnShow != null )
            {
                OnShow( this );
            }

            return true;
        }

        public void Accept()
        {
            Complete( Result.Accept );
        }

        public void Cancel()
        {
            Complete( Result.Cancel );
        }

        public void Complete( Result result )
        {
            Hide( request.Value.fadeOutTime );
            if( request.Value.callback != null )
            {
                request.Value.callback( result );
            }

            if( OnClose != null )
            {
                OnClose( this, result );
            }

            request = null;
        }
    }
}
