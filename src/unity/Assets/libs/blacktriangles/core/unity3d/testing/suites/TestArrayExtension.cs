//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;

namespace blacktriangles.Testing
{
	[TestSuite("TestArrayExtension")]
	public class TestArrayExtension
		: TestSuite
	{
		// members ////////////////////////////////////////////////////////////////

		// public methods /////////////////////////////////////////////////////////
		public override void RegisterTests()
		{
			AddTest( "TestArrayExtension", BasicTest );
		}

		// private methods ////////////////////////////////////////////////////////
		private void BasicTest()
		{
			int[] intArray = new int[] { 5, 4, 3, 2, 1 };
			Assert( intArray.Contains( 3 ) );
			Assert( intArray.IndexOf( 4 ) == 1 );
			Assert( intArray.Contains( 6 ) == false );
			Assert( intArray.IndexOf( 100 ) == -1 );
			Assert( intArray.IsValidIndex( 0 ) );
			Assert( intArray.IsValidIndex( 4 ) );
			Assert( intArray.IsValidIndex( 5 ) == false );
			Assert( intArray.IsValidIndex( -1 ) == false );
			Assert( intArray.IsValidIndex( 6 ) == false );

			System.Random rng = new System.Random( 10 );
			Assert( intArray.Random( rng ) == 4, "We are using a seeded number generator so we should be able to predict the results 1" );
			Assert( intArray.Random( rng ) == 2, "We are using a seeded number generator so we should be able to predict the results 2" );
			Assert( intArray.Random( rng ) == 3, "We are using a seeded number generator so we should be able to predict the results 3" );
			Assert( intArray.Random( rng ) == 5, "We are using a seeded number generator so we should be able to predict the results 4" );
		}
	}
}

#endif
