//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Test
{
  	public abstract class SimpleTestSuite
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public int testsRun										{ get; private set; }
		public int testsFailed									{ get; private set; }
		public int testsSucceeded								{ get { return testsRun - testsFailed; } }

		// protected methods //////////////////////////////////////////////////
		protected virtual void StartTests()
		{
		}

		protected virtual IEnumerator StartEnumeratorTests()
		{
			yield return null;
		}

		protected virtual void OnTestsComplete()
		{
		}

		protected void Assert( bool test, string message )
		{
			++testsRun;
			if( !test )
			{
				++testsFailed;
				DebugUtility.Error( message );
			}
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Start()
		{
			testsRun = 0;
			testsFailed = 0;
			StartTests();
			StartCoroutine( RunEnumeratorTests(()=>{
					TestsComplete();
				}));
		}

		// private methods ////////////////////////////////////////////////////
		private IEnumerator RunEnumeratorTests( System.Action callback )
		{
			yield return StartEnumeratorTests();
			callback();
		}

		private void TestsComplete()
		{
			DebugUtility.Log( System.String.Format( "Run: {0} | Failed: {1}", testsRun, testsFailed ) );
			OnTestsComplete();
		}
	}
}

#endif
