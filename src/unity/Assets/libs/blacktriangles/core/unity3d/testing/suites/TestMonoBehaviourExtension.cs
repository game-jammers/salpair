//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using System.Collections;

namespace blacktriangles.Testing
{
    [TestSuite("TestMonoBehaviourExtension")]
    public class TestMonoBehaviourExtension
        : TestSuite
    {
        // members ////////////////////////////////////////////////////////////////

        // public methods /////////////////////////////////////////////////////////
        public override void RegisterTests()
        {
            AddCoroutineTest( "TestMonoBehaviourExtension", BasicTest );
        }

        // private methods ////////////////////////////////////////////////////////
        private IEnumerator BasicTest()
        {
        	bool didInvoke = false;
        	float startTime = Time.time;

        	// we can ask any behaviour to call a function or action after some arbitrary delay
        	// here we ask to call this anonymous function that sets the didInvoke bool to true
        	// after 1 second.
        	runner.DelayedCall( ()=>{ didInvoke = true; }, 1f );

        	// lets wait a maximum of 5 seconds for the didInvoke flag to be set.
        	float elapsed = 0f;
        	while( !didInvoke && elapsed < 5f )
        	{
        		yield return new WaitForSeconds( 0f );
        		elapsed = Time.time - startTime;
        	}

        	// if we got here its either because we timed out (bad)
        	// or the didInvoke flag was set (good).
        	Assert( didInvoke );
        	Assert( elapsed < 5f );

        	// we should've waited at least 1 second before this happened.
        	Assert( elapsed > 1f );

        }
    }
}

#endif
