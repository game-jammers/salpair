//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using blacktriangles.Network;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Testing
{
	[TestSuite("TestNetwork")]
	public class TestNetwork
	   : TestSuite
	{
    	// constants ///////////////////////////////////////////////////////////
    	private static readonly string kHostName            = "localhost";
    	private static readonly int kHostPort               = 30000;

    	// public methods //////////////////////////////////////////////////////
    	public override void RegisterTests()
    	{
    		AddCoroutineTest( "Connect", ConnectTest );
    		AddCoroutineTest( "PacketTest", PacketTest );
    	}

    	// private methods ////////////////////////////////////////////////////////
    	private IEnumerator ConnectTest()
    	{
    		// create the server
    		SocketServer server = null;
    		Connection client = null;

            SocketServerConfig config = SocketServerConfig.Default();
            config.port = kHostPort;
            config.bufferSize = 512;
            config.connConfig.bufferSize = 16;
    		server = new SocketServer();
    		server.Start(config);

    		// create the client
    		client = new Connection();

    		// the client is not yet connected
    		Assert( client.isConnected == false );
    		Assert( client.canWriteToConnection == false );

    		// connect to the host
    		client.Connect( kHostName, kHostPort );

    		// wait to connect or timeout
    		float startTime = Time.time;
    		while( client.isConnected == false || Time.time - startTime < 1.0f )
    		{
    			yield return new WaitForSeconds( 0.1f );
    		}

    		// verify we have connected
    		Assert( client.isConnected );
    		Assert( client.canWriteToConnection );

    		// disconnect
    		client.Disconnect();
    		server.Stop();

    		// the client is no longer connected
    		Assert( client.isConnected == false );
    		Assert( client.canWriteToConnection == false );

    		server = null;
    		client = null;
    	}

    	private IEnumerator PacketTest()
    	{
            // create the server
            SocketServer server = null;
            Connection client = null;

            SocketServerConfig config = SocketServerConfig.Default();
            config.port = kHostPort;
            config.bufferSize = 512;
            config.connConfig.bufferSize = 16;

            server = new SocketServer();
            server.Start(config);

            // create the client
            client = new Connection();

            // the client is not yet connected
            Assert( client.isConnected == false );
            Assert( client.canWriteToConnection == false );

            // connect to the host
            client.Connect( kHostName, kHostPort );

            // wait to connect or timeout
            float startTime = Time.time;
            while( client.isConnected == false || Time.time - startTime < 1.0f )
            {
             yield return new WaitForSeconds( 0.1f );
            }

            // verify we have connected
            Assert( client.isConnected );
            Assert( client.canWriteToConnection );

            //
            // ##################################################################
            //
                //
                // Client Send Message to Server
                //
                {
                	JsonObject json = new JsonObject();
                	json["msg"] = "Hello World";
                    json["biggerThanBuffer"] = new byte[32];
                	client.SendPacket( new JsonPacket( json ) );
                }

                //
                // Server Receive Message From Client
                //
                {
                	List<RawPacket> packets = null;
                	startTime = Time.time;
                	while( (packets == null || packets.Count <= 0) && Time.time - startTime < 10.0f )
                	{
                		yield return new WaitForSeconds( 0.1f );
                		packets = server.TakePackets();
                	}

                	try
                	{
                		// normally you'd use a router at this point, but since we know
                		// what we are getting, here we go!
                		Assert( packets != null );
                		Assert( packets.Count == 1 );

                		RawPacket packet = packets[0];
                		JsonPacket jsonPacket = new JsonPacket();
                		jsonPacket.Decode( packet );
                		Assert( jsonPacket.json.GetField<string>("msg") == "Hello World" );

                        // now we sepond a response
                        JsonObject res = new JsonObject();
                        res["response"] = "Greetings humans";
                        packet.source.SendPacket( new JsonPacket( res ) );
                	}
                	catch( System.Exception ex )
                	{
                		Assert( false );
                		Debug.LogError("Caught unhandled exception " + ex.Message );
                	}
                }

                //
                // Client receive message from server
                //
                {
                    List<RawPacket> packets = null;
                    startTime = Time.time;
                    while( (packets == null || packets.Count <= 0) && Time.time - startTime < 10.0f )
                    {
                        yield return new WaitForSeconds(0.1f);
                        packets = client.TakePackets();
                    }

                    try
                	{
                		// normally you'd use a router at this point, but since we know
                		// what we are getting, here we go!
                		Assert( packets != null );
                		Assert( packets.Count == 1 );

                		RawPacket packet = packets[0];
                		JsonPacket jsonPacket = new JsonPacket();
                		jsonPacket.Decode( packet );
                		Assert( jsonPacket.json.GetField<string>("response") == "Greetings humans" );
                	}
                	catch( System.Exception ex )
                	{
                		Assert( false );
                		Debug.LogError("Caught unhandled exception " + ex.Message );
                	}
                }

            // DO IT AGAIN
            //
            // #################################################################
            //

                 //
                 // Client Send Message to Server
                 //
                 {
                 	JsonObject json = new JsonObject();
                 	json["msg"] = "Hello World";
                 	client.SendPacket( new JsonPacket( json ) );
                 }

                 //
                 // Server Receive Message From Client
                 //
                 {
                 	List<RawPacket> packets = null;
                 	startTime = Time.time;
                 	while( (packets == null || packets.Count <= 0) && Time.time - startTime < 10.0f )
                 	{
                 		yield return new WaitForSeconds( 0.1f );
                 		packets = server.TakePackets();
                 	}

                 	try
                 	{
                 		// normally you'd use a router at this point, but since we know
                 		// what we are getting, here we go!
                 		Assert( packets != null );
                 		Assert( packets.Count == 1 );

                 		RawPacket packet = packets[0];
                 		JsonPacket jsonPacket = new JsonPacket();
                 		jsonPacket.Decode( packet );
                 		Assert( jsonPacket.json.GetField<string>("msg") == "Hello World" );

                         // now we sepond a response
                         JsonObject res = new JsonObject();
                         res["response"] = "Greetings humans";
                         packet.source.SendPacket( new JsonPacket( res ) );
                 	}
                 	catch( System.Exception ex )
                 	{
                 		Assert( false );
                 		Debug.LogError("Caught unhandled exception " + ex.Message );
                 	}
                 }

                 //
                 // Client receive message from server
                 //
                 {
                     List<RawPacket> packets = null;
                     startTime = Time.time;
                     while( (packets == null || packets.Count <= 0) && Time.time - startTime < 10.0f )
                     {
                         yield return new WaitForSeconds(0.1f);
                         packets = client.TakePackets();
                     }

                     try
                 	{
                 		// normally you'd use a router at this point, but since we know
                 		// what we are getting, here we go!
                 		Assert( packets != null );
                 		Assert( packets.Count == 1 );

                 		RawPacket packet = packets[0];
                 		JsonPacket jsonPacket = new JsonPacket();
                 		jsonPacket.Decode( packet );
                 		Assert( jsonPacket.json.GetField<string>("response") == "Greetings humans" );
                 	}
                 	catch( System.Exception ex )
                 	{
                 		Assert( false );
                 		Debug.LogError("Caught unhandled exception " + ex.Message );
                 	}
                 }

            //
            // #################################################################
            //

             // disconnect
             client.Disconnect();
             server.Stop();

             // the client is no longer connected
             Assert( client.isConnected == false );
             Assert( client.canWriteToConnection == false );

             server = null;
             client = null;
        }
    }
}

#endif
