//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Test
{
	public abstract class TestSuite
	{
		// types //////////////////////////////////////////////////////////////
		public struct Test
		{
			public string name;
			public System.Func<IEnumerator> method;
			public System.Action action;
			public Test( string _name, System.Func<IEnumerator> _method )
			{
				name = _name;
				method = _method;
				action = null;
			}

			public Test( string _name, System.Action _action )
			{
				name = _name;
				method = null;
				action = _action;
			}
		}

		public struct TestResult
		{
            public int testsRun;
			public int checksRun;
			public int checksFailed;
		}

		// members ////////////////////////////////////////////////////////////
		public TestRunner runner								{ get; private set; }
        public int totalTests                                   { get { return tests.Count; } }
		private List<Test> tests								= new List<Test>();
        private TestResult currentResult                        = new TestResult();

		public string name										{ get { return testSuiteAttrib.name; } }
		public TestSuiteAttribute testSuiteAttrib				{ get { return GetTestSuiteAttribute(); } }
		private TestSuiteAttribute _testSuiteAttrib				= null;

		// constructor / destructor ///////////////////////////////////////////
		public void Initialize( TestRunner _runner )
		{
			runner = _runner;
			RegisterTests();
		}

		// public methods /////////////////////////////////////////////////////
		public abstract void RegisterTests();

        public void StartTests( System.Action<TestResult> onComplete )
		{
            runner.StartCoroutine( StartTestCoroutine( onComplete ) );
		}

        // protected methods //////////////////////////////////////////////////
		protected virtual void AddTest( string name, System.Action method )
		{
			Test newTest = new Test( name, method );
			tests.Add( newTest );
		}

		protected virtual void AddCoroutineTest( string name, System.Func<IEnumerator> method )
		{
			Test newTest = new Test( name, method );
			tests.Add( newTest );
		}

		protected virtual IEnumerator StartTestCoroutine( System.Action<TestResult> onComplete )
		{
            currentResult = new TestResult();
            currentResult.testsRun = 0;
            currentResult.checksRun = 0;
            currentResult.checksFailed = 0;

			foreach( Test test in tests )
			{
				if( test.method != null )
				{
					yield return runner.StartCoroutine( test.method() );
				}
				else
				{
					test.action();
				}

                ++currentResult.testsRun;
			}

			onComplete( currentResult );
		}

		public virtual void Assert( bool condition, string message = "ASSERT" )
		{
			if( !condition )
			{
				++currentResult.checksFailed;
				Debug.LogError( message );
			}

			++currentResult.checksRun;
		}

		// private methods ////////////////////////////////////////////////////////
		private TestSuiteAttribute GetTestSuiteAttribute()
		{
			if( _testSuiteAttrib == null )
			{
				_testSuiteAttrib = AssemblyUtility.GetAttribute<TestSuiteAttribute>( GetType() );
			}

			return _testSuiteAttrib;
		}
	}
}

#endif
