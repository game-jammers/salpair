//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Testing
{
	[TestSuite("TestListExtension")]
	public class TestListExtension
		: TestSuite
	{
		// members ////////////////////////////////////////////////////////////////

		// public methods /////////////////////////////////////////////////////////
		public override void RegisterTests()
		{
			AddTest( "TestListExtension", BasicTest );
		}

		// private methods ////////////////////////////////////////////////////////
		private void BasicTest()
		{
			List<int> intList = new List<int>();

			// a new list has no valid indicies
			Assert( intList.IsValidIndex(0) == false );
			Assert( intList.IsValidIndex(-1) == false );

			// an attempt to swap items that are not valid fails silently.
			intList.SwapItems( 1, 2 );

			// lets add an item
			intList.Add( 1 );

			// we now have one valid index
			Assert( intList.IsValidIndex(0) );
			Assert( intList.IsValidIndex(-1) == false );
			Assert( intList.IsValidIndex(1) == false );

			// lets add another item
			intList.Add( 2 );

			// after we swap indicies 0 and 1 they should be swapped
			intList.SwapItems( 0, 1 );
			Assert( intList.IsValidIndex(0) );
			Assert( intList.IsValidIndex(1) );
			Assert( intList[0] == 2 );
			Assert( intList[1] == 1 );

			// we can ensure a minimum length
			intList.SetMinimumLength( 3 );
			Assert( intList.Count == 3 );
			// the new items come in as default(T), in this case 0.
			Assert( intList[2] == 0 );

			intList.SetMinimumLength( 10 );
			Assert( intList.Count == 10 );
			Assert( intList[9] == 0 );

			// minimum length does nothing if it's already larget
			intList.SetMinimumLength( 5 );
			Assert( intList.Count == 10 );

			intList.SetMinimumLength( 0 );
			Assert( intList.Count == 10 );

			// this will never do anything
			intList.SetMinimumLength( -1 );
			Assert( intList.Count == 10 );

			// we can ensure a maximum length, removing items that go over
			intList.SetMaximumLength( 5 );
			Assert( intList.Count == 5 );

			// setting a maximum larger than the current length does nothing
			intList.SetMaximumLength( 100 );
			Assert( intList.Count == 5 );

			// setting a maximum less than or equal to 0 clears the list.
			intList.SetMaximumLength( 0 );
			Assert( intList.Count == 0 );

			// lets add a few items to test negative max length
			intList.AddRange( new int[]{ 1,2,3,4,5 } );
			Assert( intList.Count == 5 );

			// setting a maximum less than or equal to 0 clears the list.
			intList.SetMaximumLength( -100 );
			Assert( intList.Count == 0 );

			// set length will force a length of a certain size, adding or removing as necessary
			intList.SetLength( 100 );
			Assert( intList.Count == 100 );
			intList.SetLength( 100 );
			Assert( intList.Count == 100 );
			intList.SetLength( 99 );
			Assert( intList.Count == 99 );
			intList.SetLength( 101 );
			Assert( intList.Count == 101 );
			intList.SetLength( -1 );
			Assert( intList.Count == 0 );
		}
	}
}

#endif
