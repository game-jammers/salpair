//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using blacktriangles;

namespace blacktriangles.Test
{
	public class TestRunner
	 : MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////////
		public BaseRegistry<string, TestSuite, TestSuiteAttribute> testSuites = null;
		public bool[] runTests                                      = null;

		// unity callbacks ////////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			Initialize();
		}

		protected virtual void Start()
		{
			Debug.Log( "STARTING TESTS..." );
			RunAllTests();
		}

		// public methods /////////////////////////////////////////////////////////
		public void Initialize()
		{
			testSuites = new BaseRegistry<string, TestSuite, TestSuiteAttribute>();
			testSuites.Initialize( (attrib)=>{ return attrib.name; } );
		}

		public void RunAllTests()
		{
			StartCoroutine( DoRunAllTests() );
		}

		// private methods ////////////////////////////////////////////////////////
		private IEnumerator DoRunAllTests()
		{
			UnityEditor.EditorUtility.ClearProgressBar();

			TestSuite.TestResult total;
            total.testsRun = 0;
			total.checksRun = 0;
			total.checksFailed = 0;

            int suitesSkipped = 0;

            // initialize tests
            int totalTestsToRun = 0;
            List<TestSuite> suites = new List<TestSuite>();
            foreach( string key in testSuites.registry.Keys )
            {
				TestSuite suite = testSuites.Instantiate( key );
				suite.Initialize( this );
                totalTestsToRun += suite.totalTests;
                suites.Add( suite );
            }

            // run tests
			for( int activeIndex = 0; activeIndex < suites.Count; ++activeIndex )
			{
                TestSuite suite = suites[ activeIndex ];
				bool testComplete = false;
				float perc = total.testsRun / (float)totalTestsToRun;
				UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Running {0}/{1}", total.testsRun, totalTestsToRun ), suite.name, perc );

				if( !runTests.IsValidIndex( activeIndex ) || !runTests[activeIndex] )
				{
                    Debug.Log("Skipping suite: " + suite.name);
					++suitesSkipped;
					continue;
				}

				suite.StartTests( (res)=> {
                        total.testsRun += res.testsRun;
						total.checksRun += res.checksRun;
						total.checksFailed += res.checksFailed;
						testComplete = true;
					});

				while( testComplete == false ) yield return new WaitForSeconds( 0f );
			}

			int totalRun = suites.Count - suitesSkipped;

			UnityEditor.EditorUtility.ClearProgressBar();
			Debug.Log( System.String.Format( "Suites Run: {0} | Suites Skipped: {1} | Checks Run: {2} | Checks Failed: {3}", totalRun, suitesSkipped, total.checksRun, total.checksFailed ) );
		}
	}
}

#endif
