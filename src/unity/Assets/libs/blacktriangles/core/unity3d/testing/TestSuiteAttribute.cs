//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using blacktriangles;

namespace blacktriangles.Test
{
	[System.AttributeUsage( System.AttributeTargets.Class )]
	public class TestSuiteAttribute
		: System.Attribute
	{
		// members ////////////////////////////////////////////////////////////
		public string name                    											{ get; private set; }

		// constructor / destructor ///////////////////////////////////////////
		public TestSuiteAttribute( string _name )
		{
			name = _name;
		}
	}
}

#endif
