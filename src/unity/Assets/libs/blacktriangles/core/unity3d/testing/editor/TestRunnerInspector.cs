//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEditor;
using UnityEngine;
using blacktriangles.Test;

[CustomEditor( typeof(TestRunner) )]
public class TestRunnerInspector
	: Editor
{
	public virtual void OnEnable()
	{
		TestRunner runner = target as TestRunner;
		runner.Initialize();
	}

	public override void OnInspectorGUI()
	{
		TestRunner runner = target as TestRunner;
		bool[] actives = new bool[ runner.testSuites.registry.Count ];
		if( runner.runTests != null )
		{
			System.Array.Copy( runner.runTests, 0, actives, 0, runner.runTests.Length );
		}

		GUILayout.BeginVertical( GUI.skin.box );
			GUILayout.Label( "Test Suites" );

			int index = 0;
			foreach( string key in runner.testSuites.registry.Keys )
			{
				actives[index] = EditorGUILayout.Toggle( key, actives[ index ] );
				++index;
			}

			GUILayout.BeginHorizontal( GUI.skin.box );
				if( GUILayout.Button( "Check All" ) )
				{
					for( int i = 0; i < actives.Length; ++i )
					{
						actives[i] = true;
					}
				}

				if( GUILayout.Button( "Uncheck All" ) )
				{
					for( int i = 0; i < actives.Length; ++i )
					{
						actives[i] = false;
					}
				}
			GUILayout.EndHorizontal();
		GUILayout.EndVertical();

		GUILayout.BeginVertical( GUI.skin.box );
			if( GUILayout.Button( "Clear" ) )
			{
				UnityEditor.EditorUtility.ClearProgressBar();
			}
		GUILayout.EndVertical();

		runner.runTests = actives;
		EditorUtility.SetDirty( runner );
	}
}
