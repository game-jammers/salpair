//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class StringTable
	{
		// constants //////////////////////////////////////////////////////////
		public static readonly string kEditorStringTablePath = "data/strings/default";

		// members ////////////////////////////////////////////////////////////
		private static bool isLoaded							= false;
		private static Dictionary< string, string > dictionary	= null;

		// public methods /////////////////////////////////////////////////////
		public static bool LoadFile( string assetPath )
		{
			TextAsset asset = Resources.Load( assetPath, typeof( TextAsset ) ) as TextAsset;
			if( asset == null )
			{
				return false;
			}

			return Load( asset.text );
		}

		public static bool Load( string data )
		{
			dictionary = new Dictionary< string, string >();

			string[] lines = data.Split( '\n' );
			foreach( string line in lines )
			{
				if( line.StartsWith( "//" ) ) continue;

				int pivot = line.IndexOf( ',' );
				if( pivot >= 0 )
				{
					string id = line.Substring( 0, pivot );
					string val = line.Substring( pivot+1, line.Length-pivot-1 );
					dictionary[id] = val;
				}
			}

			isLoaded = true;

			return isLoaded;
		}

		public static string Format( string id, params string[] strings )
		{
			return System.String.Format( dictionary[id], strings );
		}
	}
}
