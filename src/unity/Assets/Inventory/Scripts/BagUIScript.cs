﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class BagUIScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    Vector2 StartOffset;

    public void OnBeginDrag(PointerEventData eventData)
    {
        StartOffset =  new Vector2(transform.position.x, transform.position.y) - eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position + StartOffset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        WindowManager.OpenBag.WindowPosition = (transform as RectTransform).position;
    }
}
