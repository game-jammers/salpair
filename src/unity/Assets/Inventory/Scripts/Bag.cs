﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Bag : Item, IDropHandler
{
    public int Size = 10;
    public List<Item> Items = new List<Item>();
    public Vector2 WindowPosition = new Vector2(0, 0);

    public void Open()
    {
        WindowManager.DisplayBagInWindow(this);
    }

    public void Awake()
    {
        for(int i = 0; i < Items.Count; i++)
        {
            Items[i] = Instantiate(Items[i].gameObject).GetComponent<Item>();
            Items[i].bag = this;
        }

        int c = Items.Count;
        for (int i = 0; i < Size - c; i++)
        {
            Items.Add(null);
        }
    }

    public void Clicked(int param)
    {
        Open();
    }

    public void OnDrop(PointerEventData eventData)
    {
        GameObject dropped = Item.draggedItem;
        Item item = dropped.GetComponent<Item>();
        if(item)
        {
            if(item.bag != this)
            {
                if (CanAddItem(item))
                {
                    if(item.bag.RemoveItemFromBag(item))
                    {
                        AddItemToBag(item);
                        item.transform.SetParent(null, false);
                    }
                }
            }
        }
    }

    public bool RemoveItemFromBag(Item item)
    {
        int index = Items.IndexOf(item);
        if(index >= 0)
        {
            Items[index] = null;
            return true;
        }
        else
        {
            Debug.LogWarning("Error: Item could not be Removed, because it was not in the " +
                "Bag in the first palce.");
            return false;
        }
    }

    public bool AddItemToBag(Item item)
    {
        for(int i = 0; i < Items.Count; i++)
        {
            if(Items[i] == null)
            {
                Items[i] = item;
                item.bag = this;
                return true;
            }
        }
        return false;
    }

    public bool AddItemToBag(Item item, int index)
    {
        if (Items[index] == null)
        {
            Items[index] = item;
            item.bag = this;
            return true;
        }
        return false;
    }

    public bool CanAddItem(Item item)
    {
        for(int i = 0; i < Items.Count; i++)
        {
            if (Items[i] == null)
                return true;
        }
        return false;
    }
}
