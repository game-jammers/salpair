﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BagHolder : MonoBehaviour
{
    public static int BagSpace = 4;
    public static List<Bag> Bags = new List<Bag>();
    private static List<GameObject> BagHolders = new List<GameObject>();

    public void Awake()
    {

        int c = Bags.Count;
        for(int i = 0; i < BagSpace - c; i++)
        {
            Bags.Add(null);
        }
        
        for (int i = 0; i < BagSpace; i++)
        {
            GameObject itemHolder = (GameObject)Instantiate(Resources.Load("ItemHolder"));
            itemHolder.transform.SetParent(transform, false);
            ItemHolderUI itemHolderUI = itemHolder.GetComponent<ItemHolderUI>();
            itemHolderUI.type = HolderType.Bag;
            itemHolderUI.BagIndex = i;

            BagHolders.Add(itemHolder);
        }

        GameObject defaultBag = (GameObject)Instantiate(Resources.Load("Bag"));
        AddBag(defaultBag.GetComponent<Bag>());

        
        AddItemToAnyBag((Instantiate(Resources.Load("CableItem") as GameObject).GetComponent<Item>()));
        AddItemToAnyBag((Instantiate(Resources.Load("IronBrickItem") as GameObject).GetComponent<Item>()));
        AddItemToAnyBag((Instantiate(Resources.Load("IronItem") as GameObject).GetComponent<Item>()));
        AddItemToAnyBag((Instantiate(Resources.Load("StoneItem") as GameObject).GetComponent<Item>()));
    }

    public static bool AddBag(Bag bag)
    {
        for(int i = 0; i < Bags.Count; i++)
        {
            Bag curBag = Bags[i];
            if(!curBag)
            {
                Bags[i] = bag;
                bag.transform.SetParent(BagHolders[i].transform, false);
                return true;
            }
        }
        return false;
    }

    public static bool AddItemToAnyBag(Item item)
    {
        foreach(Bag bag in Bags)
        {
            if(bag.AddItemToBag(item))
            {
                return true;
            }
        }
        return false;
    }
}
