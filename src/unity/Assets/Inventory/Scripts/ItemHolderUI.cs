﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public enum HolderType { Bag, Item}

public class ItemHolderUI : MonoBehaviour, IDropHandler
{
    public HolderType type = HolderType.Bag;
    public int BagIndex = 0;

    public void Awake()
    {
        Image img = GetComponent<Image>();
        img.CrossFadeAlpha(100.0f / 255.0f, 0.0f, false);
    }

    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
                return transform.GetChild(0).gameObject;
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        //if there is no item at the desired Slot
        if(!item)
        {
            //type Item can hold everything, Bag can only hold Bags
            if(type == HolderType.Item || Item.draggedItem.GetComponent<Bag>() && type == HolderType.Bag)
            {
                //Check that we're not trying to drag the bag into itself :D
                Bag bag = Item.draggedItem.GetComponent<Bag>();
                if(WindowManager.OpenBag != bag)
                {
                    //Make the item stay in UI Position
                    Item.draggedItem.transform.SetParent(transform);

                    //Get the dragged Item Component
                    Item itemItem = Item.draggedItem.GetComponent<Item>();
                    if(type == HolderType.Item) //We dragged any Item to a Itemspace
                    {
                        if (itemItem && itemItem.bag != null)
                        {
                            //Get the old index of the Item and Move it
                            int index = itemItem.bag.Items.IndexOf(itemItem);
                            if (index >= 0)
                            {
                                itemItem.bag.Items[index] = null;
                                itemItem.bag.Items[BagIndex] = itemItem;
                            }
                            else
                            {
                                Debug.LogError("Error, Item " + itemItem.name + " Was not in bags Itemlist");
                            }
                        }
                        else
                        {
                            WindowManager.OpenBag.AddItemToBag(itemItem, BagIndex);
                        }
                    }
                    else //We dragged a Bag to a Bagspace
                    {
                        //We only need to change something, if the Bag was inside a Bag before
                        if(itemItem.bag != null)
                        {
                            int index = itemItem.bag.Items.IndexOf(itemItem);
                            if (index >= 0)
                            {
                                itemItem.bag.Items[index] = null;
                                itemItem.bag = null;
                            }
                            else
                            {
                                Debug.LogError("Error, Item " + itemItem.name + " Was not in bags Itemlist");
                            }
                        }
                    }
                }
            }
        }   
    }

    public void MouseEnter()
    {
        Image img = GetComponent<Image>();
        img.CrossFadeAlpha(240.0f / 255.0f, 0.2f, false);
    }

    public void MouseExit()
    {
        Image img = GetComponent<Image>();
        img.CrossFadeAlpha(100.0f / 255.0f, 0.2f, false);
    }
}
