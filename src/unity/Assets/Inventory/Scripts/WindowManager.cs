using blacktriangles.Salpair;
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class WindowManager : MonoBehaviour
{
    public static GameObject BagWindow;
    public static Bag OpenBag;
    private static GameObject DescriptionObject;

    public void Start()
    {
        DescriptionObject = GameObject.Find("Description");
        DescriptionObject.SetActive(false);
        DescriptionObject.transform.SetParent(null, false);
    }

    public static void DisplayBagInWindow(Bag bag)
    {
        //If we opened the same Bag again, Close the window Instead
        if(OpenBag == bag)
        {
            CloseBagWindow();
            return;
        }

        //If we Opened a different bag, make sure, the old one is closed correctly
        if(OpenBag != null)
        {
            CloseBagWindow();
        }

        OpenBag = bag;
        Database.audio.PlayRandomClip( "OpenInventory", SceneController.instance.sceneCamPos, 1.0f );

        //Load the Bag from Resource if neccesary
        if (!BagWindow)
        {
            BagWindow = (GameObject)Instantiate(Resources.Load("BagUi"));
        }
        int sizeMultiplier = (int)Math.Ceiling((decimal)bag.Size / 7);

        //Display the Bagwindow on the Canvas
        Canvas canvas = GameObject.Find("InventoryUI").GetComponent<Canvas>();
        BagWindow.transform.SetParent(canvas.transform, false);
        RectTransform rectTransform = BagWindow.transform as RectTransform;
        rectTransform.sizeDelta = new Vector2(240, 32 * sizeMultiplier + 10);
        if (bag.WindowPosition.x == 0 && bag.WindowPosition.y == 0)
        {
            rectTransform.localPosition = new Vector3(-179, rectTransform.sizeDelta.y + 60);
        }
        else
        {
            rectTransform.localPosition = new Vector3(bag.WindowPosition.x - 1116, bag.WindowPosition.y);
        }
        rectTransform.anchorMin = new Vector2(1, 0);
        rectTransform.anchorMax = new Vector2(1, 0);
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        rectTransform.localScale = new Vector3(1, 1);

        //Create the empty Item Holder for the amount of Slots
        List<GameObject> itemHolders = new List<GameObject>();
        for(int i = 0; i < bag.Size; i++)
        {
            GameObject itemHolder = (GameObject)Instantiate(Resources.Load("ItemHolder"));
            itemHolder.transform.SetParent(BagWindow.transform, false);
            ItemHolderUI itemHolderUI = itemHolder.GetComponent<ItemHolderUI>();
            itemHolderUI.type = HolderType.Item;
            itemHolderUI.BagIndex = i;

            itemHolders.Add(itemHolder);
        }

        //Fill the Holder at the desired Positions with Items
        for (int i = 0; i < bag.Items.Count; i++)
        {
            GameObject itemHolder = itemHolders[i];
            if(itemHolder != null)
            {
                Item item = bag.Items[i];
                if(item != null)
                {
                    item.transform.SetParent(itemHolder.transform, false);
                }
            }
        }
    }

    public static void CloseBagWindow()
    {
        Database.audio.PlayRandomClip( "CloseInventory", SceneController.instance.sceneCamPos, 1.0f );
        for (int i = 0; i < OpenBag.Items.Count; i++)
        {
            if(OpenBag.Items[i] != null)
            {
                OpenBag.Items[i].transform.SetParent(null, false);
            }
        }
        Destroy(BagWindow);
        BagWindow = null;
        OpenBag = null;
    }

    public static void ShowToolTip(Item item)
    {
        if(!DescriptionObject)
        {
            DescriptionObject = GameObject.Find("Description");
        }
        Text textName = DescriptionObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        textName.text = item.Name;
        Text textDescription = DescriptionObject.transform.GetChild(1).gameObject.GetComponent<Text>();
        textDescription.text = item.Description;
        Canvas canvas = GameObject.Find("InventoryUI").GetComponent<Canvas>();
        DescriptionObject.transform.SetParent(canvas.transform, false);
    }

    public static void HideToolTip(Item item)
    {
        if (!DescriptionObject)
        {
            DescriptionObject = GameObject.Find("Description");
        }
        DescriptionObject.transform.SetParent(null, false);
    }
}
