﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class Item : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject draggedItem;
    private Transform oldParent;
    //private Vector3 startPosition;
    private CanvasGroup canvasGroup;

    public String ResourceName = "DefaultItem";
    public String Name = "DefaultName";
    public String Description = "DefaultDescription";

    public Bag bag = null;

    public bool stackable = false;

    [HideInInspector]
    public int stackCount
    {
        set
        {
            //if (value > 1)
            //    showStackCount = true;
        }
    }
    //private bool showStackCount = false;


    public void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        stackCount = 1;
        //Image img = GetComponent<Image>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        draggedItem = gameObject;
        //startPosition = transform.position;
        oldParent = transform.parent;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        draggedItem = null;
        if(oldParent == transform.parent)
            transform.position = transform.parent.position;
    }

    public void MouseEnter()
    {
        WindowManager.ShowToolTip(this);
    }

    public void MouseLeave()
    {
        WindowManager.HideToolTip(this);
    }
}
